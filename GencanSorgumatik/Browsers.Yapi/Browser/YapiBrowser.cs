﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Sorgumatik.Core.Objects;
using Sorgumatik.Extensions;
using Sorgumatik.Data.Engines;
using Sorgumatik.Data.Extensions;

using System.Windows.Navigation;
using Sorgumatik.Browsers;
using Browsers.IsimizFatura.Data.Objects;

namespace Browsers.Yapi.Browser {
    public class YapiBrowser : SiteBrowser {
        public YapiBrowser(WebBrowser browser) : base(browser, ESite.Yapi) {
        }

        #region Overrides
        protected override SiteInfo SiteInfo => new SiteInfo {
            SourceToNavigateFirst = ValuesEngine.SettingByKey("MainPageCorporational", Account.ID)?.Value,
        };

        protected override int MedianLevel => 2;

        protected override void InitiateCascade(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);
            var urlstr = url.ToString();

        }

        protected override void ExtendSession() {

        }

        protected override ISocketInput TestInputInstance() {
            return new YapiInput {
                CorporationID = "1",
                SubscriberNumber = "000021212121",
                CorporationCode = "1221"
            };
        }

        protected override ISocketInput NewInputInstance() {
            return new YapiInput();
        }

        public override void AjaxResponse(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);
        }

        public override void IFrameResponse(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);
        }

        public override void DocumentResponse(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);
            var urlstr = url.ToString();

            if (urlstr.Matches(@"\/ngc\/([0-9a-z]+)\.do\?lang=\w+").FirstOrDefault()?.Success == true) {
                InvokeBrowser(b => b.SetValueOfInput("userCode", Account.KurumKodu));
                InvokeBrowser(b => b.SetValueOfInput("userId", Account.HesapNo));
                InvokeBrowser(b => b.SetValueOfInput("password", Account.Sifre));
                InvokeBrowser(b => b.SetValueOfInput("userCode", Account.KurumKodu));
                InvokeBrowser(b => b.SubmitForm("ngcForm"));
                SetLevel(1);
            } else if (GetLevel() == 1) {
                var initUrl = ValuesEngine.SettingByKey("IndexPath", Account.ID)?.Value;
                var response = InvokeBrowser(b => b.ReadResponseFrom(initUrl, (request) => {
                    request.Method = "GET";
                    request.ContentType = "text/html; charset=utf-8";
                }));
                var location = response.Headers.GetValues("Location")?.First();
                var cookies = new List<(string name, string value)>();
                foreach (var sk in response.Headers.GetValues("Set-Cookie")) {
                    cookies.Add((name: sk.Trim().Split('=')[0], value: sk.Trim().Split('=')[1]));
                }
                response.Close();
                InvokeBrowser(b => b.NavigateSettingCookie(location, new Dictionary<string, string> {
                    { "Referrer", location },
                    { "Upgrade-Insecure-Requests", "1"},
                    { "Host", "ticari.yapikredi.com.tr"},
                    { "Connection", "keep-alive" }
                }, cookies));
                SetLanded();
            }
        }

        #endregion

    }
}
