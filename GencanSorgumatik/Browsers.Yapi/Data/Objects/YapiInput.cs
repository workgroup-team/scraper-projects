﻿using Sorgumatik.Core.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browsers.IsimizFatura.Data.Objects {

    public class YapiInput : SocketInput {

        [Description("kurum_id")]
        public string CorporationID { get; set; }
        [Description("alan_1")]
        public string SubscriberNumber { get; set; }
        [Description("alan_2")]
        public string CorporationCode { get; set; }

    }

}
