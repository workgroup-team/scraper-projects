﻿using Browsers.IsimizFatura.Browser;
using Browsers.Yapi.Browser;
using Sorgumatik.Browsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GencanSorgumatik.App.Code {
    public static class Extensions {

        public static SiteBrowser FetchMatchingBrowser(this ESite site, WebBrowser browser) {
            switch (site) {
                case ESite.Garanti:
                    return null;
                case ESite.Yapi:
                    return new YapiBrowser(browser);
                case ESite.IsimizFatura:
                    return new IsimizFaturaBrowser(browser);
                default:
                    return null;
            }
        }

    }

}
