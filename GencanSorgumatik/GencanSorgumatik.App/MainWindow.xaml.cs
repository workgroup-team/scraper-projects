﻿using GencanSorgumatik.App.Code;
using Sorgumatik.Browsers;
using Sorgumatik.Core.Objects;
using Sorgumatik.Data.Engines;
using Sorgumatik.Data.Extensions;
using Sorgumatik.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GencanSorgumatik.App {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private List<SiteBrowser> SiteBrowsers = new List<SiteBrowser>();
        private SiteBrowser CurrentSite;

        public MainWindow() {
            InitializeComponent();
            LoadAccounts();
        }

        #region Private Methods
        private void LoadAccounts() {
            var accounts = DataEngine.ReadAll<AccountModel>().Where(ac => ac.Banka != "ISBANK");
            lvAccounts.ItemsSource = accounts;
        }

        #endregion

        #region Event Handlers
        private void _MatchingSiteBrowser_SocketSent(SiteBrowser browser, string message) {
            txtInfoBlock.Dispatcher.Invoke(() => txtInfoBlock.Text = $"{Environment.NewLine}Server Response: {message}{Environment.NewLine}" + txtInfoBlock.Text);
        }

        private void _MatchingSiteBrowser_ConsoleLogInvoked(SiteBrowser browser, string message) {
            txtInfoBlock.Dispatcher.Invoke(() => txtInfoBlock.Text = $"{Environment.NewLine}Server Response: {message}{Environment.NewLine}" + txtInfoBlock.Text);
        }

        private void btnRemoveTab_Click(object sender, RoutedEventArgs e) {

        }

        private void BrowserTabs_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            var tab = e.AddedItems.OfType<TabItem>().FirstOrDefault();
            if (tab?.Content is WebBrowser site) {
                CurrentSite = SiteBrowsers.SingleOrDefault(sa => sa.Account.Aciklama == tab.Header.ToString());
            }
        }

        private void btnSwitchAccount_Click(object sender, RoutedEventArgs e) {
            if (sender is Button btn) {
                var selectedAccount = btn.DataContext as AccountModel;
                if (selectedAccount != null) {
                    var wb = new WebBrowser();
                    var t = new TabItem() {
                        Header = selectedAccount.Aciklama,
                        HeaderTemplate = tbSites.FindResource("TabHeader") as DataTemplate,
                        Content = wb
                    };
                    tbSites.Items.Add(t);
                    var s = selectedAccount.Banka.ToEnum<ESite>().FetchMatchingBrowser(wb);
                    s.SocketSent += _MatchingSiteBrowser_SocketSent;
                    s.SiteLogInvoked += _MatchingSiteBrowser_ConsoleLogInvoked;
                    tbSites.SelectedIndex = tbSites.Items.Count - 1;
                    s.StartNavigation();
                    CurrentSite = s;
                    btn.Content = "Bağlantıyı Kes";
                    btn.IsEnabled = false;
                }
            }
        }

        private void SocketServerAcceptMenuItem_Click(object sender, RoutedEventArgs e) {
            CurrentSite?.StartNavigation();
        }
        private void ChannelOpenPortMenuItem_Click(object sender, RoutedEventArgs e) {
            this.Title = CurrentSite?.OpenPort("Gencan Sorgumatik");
        }

        private void btnTestClient_Click(object sender, RoutedEventArgs e) {
            CurrentSite?.TestClient();
        }

        private void Window_Closed(object sender, EventArgs e) {
            CurrentSite?.SeizeServer();
        }
        #endregion

    }
}
