﻿using Browsers.Isbank.Browser;
using Browsers.Isbank.Data.Models;
using Sorgumatik.Browsers;
using Sorgumatik.Data.Engines;
using Sorgumatik.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Sorgumatik.Core.Objects;

namespace Isbank.App {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private IList<CustomersModel> _Customers = new List<CustomersModel>();
        private IsbankBrowser _Navigator;
        private CustomersModel SelectedCustomer;
        public CustomerSettingsModel SelectedCustomerSetting => DataEngine.ReadAll<CustomerSettingsModel>().SingleOrDefault(cs => cs.CustomerID == SelectedCustomer.ID);
        private readonly string MAC;

        public MainWindow() {
            InitializeComponent();
            MAC = GetMacAddress();
            if (DataEngine.ReadAll<CustomersModel>().Count() == 0) {
                InitAddWindow();
                SelectedCustomer = DataEngine.ReadAll<CustomersModel>().FirstOrDefault();
                lblUser.Content = SelectedCustomer?.FullName;
            }
        }

        private void InitAddWindow() {
            InitalizeBrowser();
            var addW = new CustomerAddWindow(_Navigator, MAC);
            addW.ShowDialog();
        }

        private void InitalizeBrowser() {
            _Navigator = new IsbankBrowser(IsBankWebBrowser);
        }

        private string GetMacAddress() {
            var macAddr =
                (
                    from nic in NetworkInterface.GetAllNetworkInterfaces()
                    where nic.OperationalStatus == OperationalStatus.Up
                    select nic.GetPhysicalAddress().ToString()
                ).FirstOrDefault();
            return macAddr;
        }

        #region Event Handlers
        private void _MatchingSiteBrowser_SocketSent(SiteBrowser browser, string message) {
            txtInfoBlock.Dispatcher.Invoke(() => txtInfoBlock.Text = $"{Environment.NewLine}Server Response: {message}{Environment.NewLine}" + txtInfoBlock.Text);
        }

        private void _MatchingSiteBrowser_ConsoleLogInvoked(string message) {
            txtInfoBlock.Dispatcher.Invoke(() => txtInfoBlock.Text = $"{Environment.NewLine}Server Response: {message}{Environment.NewLine}" + txtInfoBlock.Text);
        }

        private void SocketServerAcceptMenuItem_Click(object sender, RoutedEventArgs e) {
            _Navigator?.StartNavigation();
        }
        private LicenseResponse GetLicence() {
            return _Navigator?.RegisterLicense(SelectedCustomerSetting?.ApiGuid, MAC);
        }

        private void btnRunLoop_Click(object sender, RoutedEventArgs e) {
            _Navigator.StartLoop(GetLicence(), 60);
        }

        private void Window_Closed(object sender, EventArgs e) {
            _Navigator?.SeizeServer();
        }
        private void btnRunBrowser_Click(object sender, RoutedEventArgs e) {
            if (sender is Button btn) {
                btn.IsEnabled = false;
                this.Title = _Navigator?.OpenPort("İşBank Sorgumatik");
                _Navigator?.StartNavigation();
            }
        }

        private void ExitAppMenuItem_Click(object sender, RoutedEventArgs e) {
            this.Close();
        }
        #endregion

        private void CustomerSettingsMenuItem_Click(object sender, RoutedEventArgs e) {
            InitAddWindow();
        }
    }
}
