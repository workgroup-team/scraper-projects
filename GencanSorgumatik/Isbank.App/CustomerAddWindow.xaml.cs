﻿using Browsers.Isbank.Data.Models;
using Sorgumatik.Browsers;
using Sorgumatik.Data.Engines;
using Sorgumatik.Data.Extensions;
using Sorgumatik.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Isbank.App {
    /// <summary>
    /// Interaction logic for CustomerAddWindow.xaml
    /// </summary>
    public partial class CustomerAddWindow : Window {

        private readonly SiteBrowser Navigator;
        private readonly string MAC;

        private IList<CustomersModel> _Customers = new List<CustomersModel>();
        private CustomersModel SelectedCustomer;
        public CustomerSettingsModel SelectedCustomerSetting => DataEngine.ReadAll<CustomerSettingsModel>().SingleOrDefault(cs => cs.CustomerID == SelectedCustomer.ID);

        public CustomerAddWindow(SiteBrowser navigator, string mac) {
            this.Navigator = navigator;
            this.MAC = mac;
            InitializeComponent();
            InitalizeSettings();
        }
        private void InitalizeSettings() {
            cmbCustomerSettings.ItemsSource = DataEngine.ReadAll<CustomersModel>();
            cmbCustomerSettings.SelectedIndex = 0;
            btnAddCustomer.IsEnabled = DataEngine.ReadAll<CustomersModel>().Count() == 0;
        }

        private void btnAddCustomer_Click(object sender, RoutedEventArgs e) {
            if (sender is Button btn) {
                var isbank = DataEngine.ReadAll<AccountModel>().SingleOrDefault(ac => ac.Banka == "ISBANK");
                isbank.KurumsalMi = cbCorporate.IsChecked == true ? 1 : 0;
                isbank.UpsertSelf(m => m.ID);
                var model = new CustomersModel {
                    FullName = txtFullName.Text,
                    UserName = txtUserName.Text,
                    Password = txtPassword.Text,
                    CorporateNumber = txtCorporateNumber.Text
                };
                try {
                    var inserted = model.UpsertSelf(m => m.ID);
                    var setting = new CustomerSettingsModel {
                        BankAccount = txtBankAccount.Text.To<long>(),
                        ApiGuid = txtApiGuid.Text,
                        CustomerID = inserted.ID,
                        RequestCount = 1
                    }.UpsertSelf(m => m.ID);
                    if (Navigator?.RegisterLicense(txtApiGuid.Text, MAC) != null) {
                        MessageBox.Show(this, "Hesap Kaydı Başarılı.",
                        "İşlem Sonucu", MessageBoxButton.OK, MessageBoxImage.Information);
                        this.Close();
                    } else {
                        MessageBox.Show(this, $"Hesap Kaydı Başarısız.\n",
                       "Hata Oluştu", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                } catch (Exception exc) {
                    MessageBox.Show(this, $"Hesap Kaydı Başarısız.\n{exc.Message}",
                        "Hata Oluştu", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnUpdateCustomer_Click(object sender, RoutedEventArgs e) {
            if (sender is Button btn) {
                var custUpdated = SelectedCustomer?.UpsertSelf(m => m.ID);
                var updated = SelectedCustomerSetting?.UpsertSelf(m => m.ID);
                if (custUpdated != null && updated != null) {
                    _Customers.Single(c => c.ID == updated.ID).Digest(custUpdated);
                    cmbCustomerSettings.ItemsSource = _Customers.ToArray();
                    if (Navigator?.RegisterLicense(updated.ApiGuid, MAC) != null) {
                        MessageBox.Show(this, "Hesap Güncelleme Başarılı.",
                        "İşlem Sonucu", MessageBoxButton.OK, MessageBoxImage.Information);
                    } else {
                        MessageBox.Show(this, $"Hesap Güncelleme Başarısız.",
                       "Hata Oluştu", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                } else {
                    MessageBox.Show(this, $"Hesap Güncelleme Başarısız.",
                        "Hata Oluştu", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void cmbCustomerSettings_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (sender is ComboBox cmb) {
                SelectedCustomer = e.AddedItems.OfType<CustomersModel>().FirstOrDefault();
                txtBankAccount.Text = SelectedCustomerSetting?.BankAccount.ToString();
                txtApiGuid.Text = SelectedCustomerSetting?.ApiGuid;
                txtFullName.Text = SelectedCustomer.FullName;
                txtUserName.Text = SelectedCustomer.UserName;
                txtPassword.Text = SelectedCustomer.Password;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            if (DataEngine.ReadAll<CustomersModel>().Count() == 0) {
                MessageBox.Show(this, $"Henüz Bir Hesap Ekleyemediniz.",
                      "Hata Oluştu", MessageBoxButton.OK, MessageBoxImage.Error);
                e.Cancel = true;
            }
        }
    }
}
