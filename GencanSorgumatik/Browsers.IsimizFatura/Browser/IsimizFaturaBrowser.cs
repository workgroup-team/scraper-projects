﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Navigation;
using Sorgumatik.Core.Objects;
using Sorgumatik.Data.Engines;
using Sorgumatik.Extensions;
using Sorgumatik.Data.Extensions;
using System.Threading;
using Sorgumatik.Browsers;
using Browsers.IsimizFatura.Data.Objects;

namespace Browsers.IsimizFatura.Browser {
    public class IsimizFaturaBrowser : SiteBrowser {
        private string _Operator = "TURKCELL";

        public IsimizFaturaBrowser(WebBrowser browser) : base(browser, ESite.IsimizFatura) {
        }

        #region Overrides
        protected override SiteInfo SiteInfo => new SiteInfo {
            SourceToNavigateFirst = ValuesEngine.SettingByKey("MainPage", Account.ID)?.Value,
            SourceToLandingPage = ValuesEngine.SettingByKey("KontorPage", Account.ID).Value
        };

        protected override int MedianLevel => 2;

        protected override void ExtendSession() {

        }

        protected override ISocketInput NewInputInstance() {
            return new IsimizFaturaInput();
        }

        protected override ISocketInput TestInputInstance() {
            return new IsimizFaturaInput {
                CorporationID = "118",
                PhoneNumber = "5360331719"
            };
        }

        protected override void InitiateCascade(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);
            var r = new Random().NextDouble();
            NavigateBrowserTo(ValuesEngine.SettingByKey("BilgiAlPage",
                Account.ID).Value,
                new Dictionary<string, string> {
                        { "GSMNO", input["alan_1"] },
                        { "kisitlama", "undefined" },
                        { "operatoru", "bul" },
                        { "rand", r.ToString() }
                },
                new Dictionary<string, string> {
                        { "Referer", "http://bayi.isimizfatura.com/Kontor/" },
                        { "Host", "bayi.isimizfatura.com"}
                }, true
            );
        }

        public override void AjaxResponse(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);

        }

        public override void IFrameResponse(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);

        }

        public override void DocumentResponse(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);
            var urlstr = url.ToString();
            var r = new Random().NextDouble();
            var body = InvokeBrowser(b => b.GetBody());
            if (urlstr.Equals(ValuesEngine.SettingByKey("MainPage", Account.ID)?.Value)) {
                InvokeBrowser(b => b.SetValueOfInput("kullanici_adi", Account.HesapNo));
                InvokeBrowser(b => b.SetValueOfInput("password", Account.Sifre));
                InvokeBrowser(b => b.Click("commit"));
            } else if (urlstr.Equals(ValuesEngine.SettingByKey("PassPage", Account.ID)?.Value)) {
                var tokeBody = body.Matches(ValuesEngine.PatternByKey("TokeDegerler", Account.ID).Pattern)
                    .FirstOrDefault()?.Groups[1].Value;
                var tokes = tokeBody.Matches(ValuesEngine.PatternByKey("CiftTirnak", Account.ID).Pattern);
                var tokeResult = "";
                foreach (var chr in Account.KurumKodu.ToCharArray()) {
                    var i = Convert.ToInt32(chr.ToString());
                    tokeResult += tokes.AtIndexOrDefault(i).Groups[1].Value;
                }
                var token = body.Matches(ValuesEngine.PatternByKey("Token", Account.ID).Pattern)
                    .FirstOrDefault()?.Groups[1].Value;
                NavigateBrowserTo(ValuesEngine.SettingByKey("PassPage", Account.ID).Value,
                    new Dictionary<string, string> {
                        { "toke", tokeResult },
                        { "token", token },
                        { "parola", Account.KurumKodu }
                    },
                    new Dictionary<string, string> {
                        { "Referer", "http://bayi.isimizfatura.com/Kontor/" },
                        { "Host", "bayi.isimizfatura.com"}
                    }
                );
                SetLevel(1);
            } else if (GetLevel() == 1) {
                NavigateBrowserTo(SiteInfo.SourceToLandingPage);
                SetLanded();
            } else if (urlstr.FirstMatch(ValuesEngine.PatternByKey("OperatorBul", Account.ID)?.Pattern)?
               .Groups[1].Value == "bul") {
                _Operator = body.Matches(ValuesEngine.PatternByKey("Operator", Account.ID)?.Pattern)
                    .AtIndexOrDefault(1)?.Groups[1].Value;
                //SetValueOfInput("sorgu_input_0", input.SearchItem));
                NavigateBrowserTo(ValuesEngine.SettingByKey("BilgiAlPage",
                    Account.ID).Value,
                    new Dictionary<string, string> {
                        { "GSMNO", input["alan_1"] },
                        { "kisitlama", "" },
                        { "operatoru", _Operator },
                        { "rand", r.ToString() }
                    },
                    new Dictionary<string, string> {
                        { "Referer", "http://bayi.isimizfatura.com/Kontor/" },
                        { "Host", "bayi.isimizfatura.com"}
                    }, true
                );
                SetLevel(3);
            } else if (GetLevel() == 3) {
                _Operator = body.Matches(ValuesEngine.PatternByKey("Operator", Account.ID)?.Pattern)
                    .AtIndexOrDefault(1)?.Groups[1].Value;
                var tturu = body.Matches(ValuesEngine.PatternByKey("TTuru", Account.ID)?.Pattern)
                    .AtIndexOrDefault(1)?.Groups[1].Value;
                var tavsiye = body.Matches(ValuesEngine.PatternByKey("Tavsiye", Account.ID)?.Pattern)
                    .AtIndexOrDefault(1)?.Groups[1].Value;
                //SetValueOfInput("sorgu_input_0", input.SearchItem));
                NavigateBrowserTo(ValuesEngine.SettingByKey("BilgiAlPage",
                    Account.ID).Value,
                    new Dictionary<string, string> {
                        { "GSMNO", input["alan_1"] },
                        { "operatoru", _Operator },
                        { "tturu", tturu },
                        { "tavsiye", tavsiye },
                        { "rand", r.ToString() }
                    },
                    new Dictionary<string, string> {
                        { "Referer", "http://bayi.isimizfatura.com/Kontor/" },
                        { "Host", "bayi.isimizfatura.com"}
                    }, true
                );
                SetLevel(4);
            } else if (GetLevel() == 4) {
                var fiyatlar = new List<string>();
                var inputs = body.Matches(ValuesEngine.PatternByKey("KontorUrunFiyatListe", Account.ID).Pattern);
                foreach (var match in inputs) {
                    if (match.Success) {
                        var fiyat = match.Groups[1].Value.Matches(
                            ValuesEngine.PatternByKey("CiftTirnak", Account.ID).Pattern).AtIndexOrDefault(3);
                        if (fiyat?.Success == true) {
                            fiyatlar.Add(fiyat?.Groups[1].Value);
                        }
                    }
                }
                if (fiyatlar.Count > 0) {
                    OutputStack.Push("1:" + fiyatlar.Aggregate((p, n) => $"{p},{n}"));
                } else {
                    OutputStack.Push("2:abonesorgulamabasarisiz");
                }
                SetLevel(4);
            }
        }

        #endregion

    }
}
