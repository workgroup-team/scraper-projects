﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browsers.Isbank.Data.Models {
    public class CustomersModel : BaseModel<CustomersModel>, IModel {

        [Column("ID"), PK]
        public long ID { get; set; }
        [Column("FullName")]
        public string FullName { get; set; }
        [Column("UserName")]
        public string UserName { get; set; }
        [Column("Password")]
        public string Password { get; set; }
        [Column("CorporateNumber")]
        public string CorporateNumber { get; set; }

        public override string TableName => "Customers";

        public override string Database => "isbank";

    }
}
