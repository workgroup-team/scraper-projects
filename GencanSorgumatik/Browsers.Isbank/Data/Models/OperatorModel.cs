﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browsers.Isbank.Data.Models {
    public class OperatorModel : BaseModel<OperatorModel>, IModel {

        [Column("ID"), PK]
        public long ID { get; set; }
        [Column("Key")]
        public string Key { get; set; }
        [Column("Path")]
        public string Path { get; set; }
        [Column("DropdownIdentity")]
        public string DropdownIdentity { get; set; }

        public override string TableName => "Operators";

        public override string Database => "isbank";
    }
}
