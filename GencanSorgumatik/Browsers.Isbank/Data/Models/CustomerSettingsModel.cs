﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browsers.Isbank.Data.Models {
    public class CustomerSettingsModel : BaseModel<CustomerSettingsModel>, IModel {

        [Column("ID"), PK]
        public long ID { get; set; }
        [Column("BankAccount")]
        public long BankAccount { get; set; }
        [Column("ApiGuid")]
        public string ApiGuid { get; set; }
        [Column("RequestCount")]
        public long RequestCount { get; set; }
        [Column("CustomerID")]
        public long CustomerID { get; set; }

        public override string TableName => "CustomerSettings";

        public override string Database => "isbank";
    }
}
