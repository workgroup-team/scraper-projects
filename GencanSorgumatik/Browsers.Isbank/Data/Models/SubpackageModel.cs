﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browsers.Isbank.Data.Models {
    public class SubpackageModel : BaseModel<SubpackageModel>, IModel {

        [Column("ID"), PK]
        public long ID { get; set; }
        [Column("Description")]
        public string Description { get; set; }
        [Column("Value")]
        public string Value { get; set; }
        [Column("ItemOrder")]
        public long ItemOrder { get; set; }
        [Column("PackageID")]
        public long PackageID { get; set; }

        public override string TableName => "Subpackages";

        public override string Database => "isbank";
    }
}
