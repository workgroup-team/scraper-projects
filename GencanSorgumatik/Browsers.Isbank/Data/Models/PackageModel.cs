﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Browsers.Isbank.Data.Models {
    public class PackageModel : BaseModel<PackageModel>, IModel {

        [Column("ID"), PK]
        public long ID { get; set; }
        [Column("Key")]
        public string Key { get; set; }
        [Column("OptionValue")]
        public string OptionValue { get; set; }
        [Column("OperatorID")]
        public long OperatorID { get; set; }

        public override string TableName => "Packages";

        public override string Database => "isbank";
    }
}
