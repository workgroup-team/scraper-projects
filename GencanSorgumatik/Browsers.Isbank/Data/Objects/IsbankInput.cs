﻿
using Sorgumatik.Core.Objects;
using Sorgumatik.Data.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Browsers.Isbank.Data.Objects {
    public class IsbankInput : SocketInput {

        [Description("telefon")]
        public string PhoneNumber { get; set; }
        [Description("tip")]
        public string QueryType { get; set; }
        [Description("op_id")]
        public string OpID { get; set; }
        [Description("tip_id")]
        public string TypeID { get; set; }
        [Description("paket_id")]
        public string PackageOrder { get; set; }
        [Description("islem_id")]
        public string RecordID { get; set; }

    }
}
