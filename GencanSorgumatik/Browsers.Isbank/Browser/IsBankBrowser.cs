﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Navigation;
using Sorgumatik.Core.Objects;
using Sorgumatik.Data.Engines;
using Sorgumatik.Extensions;
using Sorgumatik.Data.Extensions;
using System.Threading;
using Sorgumatik.Browsers;
using Browsers.Isbank.Data.Models;
using Browsers.Isbank.Data.Objects;
using mshtml;

namespace Browsers.Isbank.Browser {
    public class IsbankBrowser : SiteBrowser {
        
        private OperatorModel _Operator;
        private PackageModel _Package;

        public IsbankBrowser(WebBrowser parent) : base(parent, ESite.Isbank, socket: false) { }

        #region Overrides
        protected override SiteInfo SiteInfo => new SiteInfo {
            SourceToNavigateFirst = ValuesEngine.SettingByKey("MainPage", Account.ID)?.Value,
            SourceToLandingPage =   ValuesEngine.SettingByKey(Account.KurumsalMi == 1 ? "CorporatePage": "MainPage", Account.ID).Value
        };

        protected override int MedianLevel => 1;

        protected override void ExtendSession() {
            var body = InvokeBrowser(b => b.GetBody());
            InvokeBrowser(b => b.ExecScript("Extender", "$('#Linkbutton1').click();"));
        }

        protected override ISocketInput NewInputInstance() {
            return new IsbankInput();
        }

        protected override ISocketInput TestInputInstance() {
            var ip = new IsbankInput();
            ip.PhoneNumber = "5377825942";
            ip.QueryType = "TL";
            ip.OpID = "1";
            ip.TypeID = "1";
            ip.PackageOrder = "2";
            ip.RecordID = "65536";
            return ip;
        }

        protected override void InitiateCascade(ISocketInput input) {
            _Operator = DataEngine.ReadAll<OperatorModel>().SingleOrDefault(op => op.ID == input["op_id"].To<long>());
            InvokeBrowser(b => b.ClickToSelector($"#{_Operator.Path} > a"));
            SetLevel(2, 2700);

            var icerik = GetIFrame("icerik");
            _Package = DataEngine.ReadAll<PackageModel>().SingleOrDefault(t => t.ID == input["tip_id"].To<long>());
            var custSetting = DataEngine.ReadAll<CustomerSettingsModel>().FirstOrDefault();
            icerik.getElementById(_Operator.DropdownIdentity).setAttribute("value", _Package.OptionValue);
            icerik.getElementById("_ctl0_GSMYuklemeGiris_cmbHesaplarList").setAttribute("value", custSetting.BankAccount.ToString().PadLeft(6, '0'));
            icerik.getElementById("_ctl0_GSMYuklemeGiris_txtCepTelNumber").setAttribute("value", input["telefon"]);
            icerik.getElementById("_ctl0_GSMYuklemeGiris_btnGonder").click();
            SetLevel(3, 2000);
            
            var subpackage = DataEngine.ReadAll<SubpackageModel>()
                    .Where(sbp => sbp.PackageID == _Package.ID).FirstOrDefault(sbp => sbp.ItemOrder == input["paket_id"].To<long>());
            var packageInput = icerik.getElementsByTagName("input").OfType<IHTMLInputElement>().FirstOrDefault(i => i.value == subpackage.Value);
            packageInput.@checked = true;
            icerik.getElementById("_ctl0_GSMYuklemeSecenekler_btnGonder").click();
            SetLevel(4, 4000);

            var hata = icerik.getElementById("_ctl0_Error_lblHataAciklamaBaslik");
            if (hata == null) {
                var lightBox = GetIFrame("InternetLightBoxFrame");
                lightBox.getElementById("_ctl0_GSMYuklemeOnay_btnGonder").click();
                //_ctl0__ctl1_lblMsgSuccessExplanation2 : İşleminiz tamamlanmıştır
                //_ctl0_GSMYuklemeSonuc_lblBakiye : 634.47 TL
                SetLevel(5, 2000);
                var desc = icerik.getElementById("_ctl0__ctl1_lblMsgSuccessExplanation2");
                var bakiye = icerik.getElementById("_ctl0_GSMYuklemeSonuc_lblBakiye");
                if (bakiye != null) {
                    OutputStack.Push($"islem=sonuc&islem_id={input["islem_id"]}&durum=1&aciklama={desc.innerText}&bakiye={bakiye.innerText.StripTrailing(" TL")}");
                } else {
                    OutputStack.Push($"islem=sonuc&islem_id={input["islem_id"]}&durum=3&aciklama={hata.innerText.Substring(0, hata.innerText.IndexOf('.'))}");
                }
            } else {
                SetLevel(5);
                OutputStack.Push($"islem=sonuc&islem_id={input["islem_id"]}&durum=3&aciklama={hata.innerText.Substring(0, hata.innerText.IndexOf('.'))}");
            }

            SetCompleted(4000);
        }

        public override void AjaxResponse(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);
        }

        public override void IFrameResponse(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);
        }

        public override void DocumentResponse(ISocketInput input) {
            Uri url = InvokeBrowser(b => b.Source);
            var urlstr = url.ToString();
            var isbankAccount = DataEngine.ReadAll<CustomersModel>().FirstOrDefault();
            if (urlstr.Equals(ValuesEngine.SettingByKey("MainPage", Account.ID)?.Value)) {
                SetLanded();
                if (Account.KurumsalMi == 1) {
                    InvokeBrowser(b => b.SetValueOfInput("CustomerNumberTextBox", isbankAccount.CorporateNumber)); 
                }
                InvokeBrowser(b => b.SetValueOfInput("_ctl0_MusNoText", isbankAccount.UserName));
                InvokeBrowser(b => b.SetValueOfInput("ParolaText", isbankAccount.Password));
                InvokeBrowser(b => b.Click("_ctl0_SubeLogin01_btnGiris"));
            }
        }

        #endregion

    }
}
