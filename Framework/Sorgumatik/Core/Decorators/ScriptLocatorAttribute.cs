﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Core.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    sealed class ScriptLocatorAttribute : Attribute {

        readonly string locator;

        public ScriptLocatorAttribute(string locator) {
            this.locator = locator;
        }

        public string Locator {
            get { return locator; }
        }

    }
}
