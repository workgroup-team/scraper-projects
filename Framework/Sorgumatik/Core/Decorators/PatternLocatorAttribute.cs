﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Core.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    sealed class PatternLocatorAttribute : Attribute {

        readonly string locator;
        readonly int stage;

        public PatternLocatorAttribute(string locator, int stage) {
            this.locator = locator;
            this.stage = stage;
        }

        public string Locator {
            get { return locator; }
        }
        public int Stage {
            get { return stage; }
        }

        public bool DoNotFlush { get; set; }

    }
}
