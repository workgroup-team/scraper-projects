﻿using Sorgumatik.Core.Objects;
using Sorgumatik.Data.Engines;
using Sorgumatik.Data.Extensions;
using Sorgumatik.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Core.Objects {
    public class SocketClient {

        private TcpClient Sender;
        private IPEndPoint EndPoint;
        public SocketClient(IPAddress serverIP, AccountModel account) {
            EndPoint = new IPEndPoint(serverIP, ValuesEngine.SettingByKey("Port", account.ID).Value.To<int>());
        }

        public void SendToSocket(ISocketInput input) {
            try {
                Sender = new TcpClient();
                Sender.Connect(EndPoint);
                var serverStream = Sender.GetStream();
                byte[] outStream = input.Serialize().DecodeBytes();
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
                Sender.Close();
            } catch {

            }
        }

        public void End() {
            Sender?.Close();
        }

    }
}
