﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Sorgumatik.Core.Objects {
    public class LicenseResponse {

        public LicenseData data { get; set; }
        
    }

    public class LicenseData {
        public string url { get; set; }
        public string error { get; set; }
    }
}
