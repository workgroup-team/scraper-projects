﻿
using Sorgumatik.Data.Extensions;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Collections.Generic;

namespace Sorgumatik.Core.Objects {

    public abstract class SocketInput : ISocketInput {

        private IDictionary<string, object> _Tag = new Dictionary<string, object>();
        public IDictionary<string, object> Tag => _Tag;

        public ISocketInput Deserialize(string data) {
            var parts = data.Split('&');
            foreach (var prop in this.GetType().PublicPropertiesOf().Where(p => p.HasAttribute<DescriptionAttribute>())) {
                var key = prop.GetCustomAttribute<DescriptionAttribute>().Description;
                prop.SetValue(this, parts.SingleOrDefault(p => p.Split('=').FirstOrDefault() == key)?
                    .Split('=').AtIndexOrDefault(1));
            }
            return this;
        }

        public string Serialize() {
            var sb = new StringBuilder();
            foreach (var prop in this.GetType().PublicPropertiesOf().Where(p => p.HasAttribute<DescriptionAttribute>())) {
                var key = prop.GetCustomAttribute<DescriptionAttribute>().Description;
                var value = prop.GetValue(this).ToString();
                sb.AppendFormat("{0}={1}&", key, value);
            }
            return sb.ToString().TrimEnd('&');
        }

        public string this[string key] {
            get => this.GetType().PublicPropertiesHavingAttribute<DescriptionAttribute>()
                .SingleOrDefault(p => p.GetCustomAttribute<DescriptionAttribute>().Description == key)?.GetValue(this).ToString();
            set => this.GetType().PublicPropertiesHavingAttribute<DescriptionAttribute>()
                .SingleOrDefault(p => p.GetCustomAttribute<DescriptionAttribute>().Description == key)?.SetValue(this, value);
        }

    }

}