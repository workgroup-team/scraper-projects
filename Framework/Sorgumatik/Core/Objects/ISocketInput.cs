﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Core.Objects {
    public interface ISocketInput {
        
        ISocketInput Deserialize(string data);
        string Serialize();
        IDictionary<string, object> Tag { get; }
        string this[string key] { get; set; }
    }
}
