﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Core.Objects {
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class ScriptObject {

        public delegate void ConsoleLogInvokedHandler(string message);
        internal event ConsoleLogInvokedHandler ConsoleLogInvoked;

        public void log(string message) {
            ConsoleLogInvoked?.Invoke(message);
        }
    }
}
