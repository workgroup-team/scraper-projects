﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Data.Models {
    public class AccountModel: BaseModel<AccountModel>, IModel {

        [Column("ID"), PK]
        public long ID { get; set; }
        [Column("BankName")]
        public string Banka { get; set; }
        [Column("IsCorporate")]
        public long KurumsalMi { get; set; }
        [Column("AccountName")]
        public string HesapAdi { get; set; }
        [Column("CorporateCode")]
        public string KurumKodu { get; set; }
        [Column("AccountNumber")]
        public string HesapNo { get; set; }
        [Column("Password")]
        public string Sifre { get; set; }
        [Column("IsActive")]
        public long AktifMi { get; set; }
        [Column("Description")]
        public string Aciklama { get; set; }

        public override string TableName => "Accounts";
        public override string Database => "db";

    }
}
