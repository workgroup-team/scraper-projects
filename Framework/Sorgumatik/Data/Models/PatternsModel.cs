﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Data.Models {
    public class PatternsModel : BaseModel<PatternsModel>, IModel {

        public override string Database => "db";
        public override string TableName => "Patterns";

        [Column("ID"), PK]
        public long ID { get; set; }
        [Column("Key")]
        public string Key { get; set; }
        [Column("Pattern")]
        public string Pattern { get; set; }
        [Column("AccountID")]
        public long AccountID { get; set; }
    }
}
