﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Data.Models {
    public class CorporateModel : BaseModel<CorporateModel>, IModel {

        public override string Database => "db";
        public override string TableName => "Corporates";

        [Column("ID"), PK]
        public long ID { get; set; }
        [Column("CorporateName")]
        public string KurumAdi { get; set; }
        [Column("BranchName")]
        public string BransAdi { get; set; }
        [Column("FilterIndex")]
        public long Index { get; set; }
        [Column("InvoiceTypeIndex")]
        public long FaturaTipIndex { get; set; }
        [Column("MaxLength")]
        public long UzunlukMax { get; set; }

    }
}
