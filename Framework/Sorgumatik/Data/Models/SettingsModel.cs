﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Data.Models {
    public class SettingsModel : BaseModel<SettingsModel>, IModel {

        public override string Database => "db";
        public override string TableName => "Settings";

        [Column("ID"), PK]
        public long ID { get; set; }
        [Column("Key")]
        public string Key { get; set; }
        [Column("Value")]
        public string Value { get; set; }
        [Column("AccountID")]
        public long AccountID { get; set; }
    }
}
