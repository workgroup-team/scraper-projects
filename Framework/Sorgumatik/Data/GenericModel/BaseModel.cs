﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.Engines;
using Sorgumatik.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Data.GenericModel {
    public abstract class BaseModel<TModel>
        where TModel : class, IModel {

        public abstract string Database { get; }
        public abstract string TableName { get; }

        public string SelectAllClause => $"SELECT * FROM {TableName}";

        public void Digest(TModel other) {
            var props = typeof(TModel).PublicPropertiesOf()
                .Where(p => p.HasAttribute<ColumnAttribute>());
            foreach (var prop in props.Where(p => !p.HasAttribute<PKAttribute>())) {
                prop.SetValue(this, prop.GetValue(other));
            }
        }
        public bool DeleteSelf<TProp>(Expression<Func<TModel, TProp>> keySelector) {
            PropertyInfo uqkey = null;
            if (keySelector.Body is MemberExpression keymem) {
                uqkey = keymem.Member as PropertyInfo;
            } else {
                throw new Exception("Selector should be member expression");
            }
            var key = uqkey.GetValue(this);
            var clause = $"DELETE FROM {TableName} WHERE [{uqkey.GetCustomAttribute<ColumnAttribute>().ColumnName}]=@__key";
            using (var eng = new SqliteEngine(Database)) {
                if (eng.ExecuteClause(clause, (cmd) => {
                    cmd.Parameters.AddWithValue("__key", key);
                }) == true) {
                    return true;
                } else {
                    throw new Exception($"DeleteSelf Failed To Delete: {clause}");
                }
            }
        }
        public TModel UpsertSelf<TProp>(Expression<Func<TModel, TProp>> keySelector) {
            var columns = new StringBuilder();
            var parameters = new StringBuilder();
            var set = new StringBuilder();
            var props = typeof(TModel).PublicPropertiesOf()
                .Where(p => p.HasAttribute<ColumnAttribute>());
            foreach (var prop in props.Where(p => !p.HasAttribute<PKAttribute>())) {
                var value = prop.GetValue(this);
                if (value.IsAssigned()) {
                    var colName = prop.GetCustomAttribute<ColumnAttribute>().ColumnName;
                    columns.AppendFormat("[{0}], ", colName);
                    parameters.AppendFormat("@{0}, ", colName);
                    set.AppendFormat("[{0}]=@{0}, ", colName);
                }
            }
            PropertyInfo uqkey = null;
            if (keySelector.Body is MemberExpression keymem) {
                uqkey = keymem.Member as PropertyInfo;
            } else {
                throw new Exception("Selector should be member expression");
            }
            var key = uqkey.GetValue(this);
            var clause1 = $"UPDATE {TableName} SET {set.ToString().StripTrailing(", ")} WHERE [{uqkey.GetCustomAttribute<ColumnAttribute>().ColumnName}]=@__key";
            var clause2 = $"INSERT INTO {TableName} ({columns.ToString().StripTrailing(", ")}) SELECT {parameters.ToString().StripTrailing(", ")} WHERE (Select Changes() = 0)";
            var finalClause = $"{clause1}; {clause2};";
            using (var eng = new SqliteEngine(Database)) {
                if (eng.ExecuteClause(finalClause, (cmd) => {
                    foreach (var p in props.Where(p => !p.HasAttribute<PKAttribute>())) {
                        var colName = p.GetCustomAttribute<ColumnAttribute>().ColumnName;
                        var value = p.GetValue(this);
                        if (value.IsAssigned()) {
                            cmd.Parameters.AddWithValue(colName, value);
                        }
                    }
                    cmd.Parameters.AddWithValue("__key", key);
                }) == true) {
                    var insertedID = eng.ExecuteIdentity();
                    if (insertedID > 0) {
                        props.SingleOrDefault(p => p.HasAttribute<PKAttribute>()).SetValue(this, insertedID);
                    }
                } else {
                    throw new Exception($"Upsert Clause Failed To Insert: {finalClause}");
                }
            }
            return this as TModel;
        }

    }
}
