﻿namespace Sorgumatik.Data.GenericModel {

    public interface IModel {

        string Database { get; }
        string TableName { get; }
        string SelectAllClause { get; }

    }

}