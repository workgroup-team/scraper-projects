﻿using Sorgumatik.Core;
using Sorgumatik.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Data.Engines {

    public class ValuesEngine {

        private static IEnumerable<SettingsModel> _Settings;
        private static IEnumerable<PatternsModel> _Patterns;
        static ValuesEngine() {
            Refresh();
        }

        public static SettingsModel SettingByKey(string key, long accountID) {
            return _Settings.SingleOrDefault(s => s.Key == key && s.AccountID == accountID);
        }

        public static PatternsModel PatternByKey(string key, long accountID) {
            return _Patterns.SingleOrDefault(s => s.Key == key && s.AccountID == accountID);
        }

        internal static void Refresh() {
            _Settings = DataEngine.ReadAll<SettingsModel>();
            _Patterns = DataEngine.ReadAll<PatternsModel>();
        }
    }

}
