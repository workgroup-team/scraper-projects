﻿using Sorgumatik.Data.Decorators;
using Sorgumatik.Data.Extensions;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Data.Engines {

    public static class DataEngine {

        public static IEnumerable<TModel> ReadAll<TModel>()
           where TModel : IModel {
            using (var eng = new SqliteEngine(Activator.CreateInstance<TModel>().Database)) {
                using (var reader = eng.SelectReaderFor<TModel>()) {
                    while (reader.Read()) {
                        var entity = Activator.CreateInstance<TModel>();
                        foreach (var prop in typeof(TModel).PublicPropertiesOf()
                            .Where(p => p.HasAttribute<ColumnAttribute>())) {
                            prop.SetValue(entity, reader[prop.GetCustomAttribute<ColumnAttribute>().ColumnName].StronglyType());
                        }
                        yield return entity;
                    }
                }
            }
        }
    }
}
