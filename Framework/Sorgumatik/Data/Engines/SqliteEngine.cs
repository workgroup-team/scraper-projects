﻿using Sorgumatik.Data.Extensions;
using Sorgumatik.Data.GenericModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Data.Engines {
    public class SqliteEngine : IDisposable {

        private SQLiteConnection connection;
        public SqliteEngine(string database) {
            connection = new SQLiteConnection(
                $@"Data Source={AppDomain.CurrentDomain.BaseDirectory}\DB\{database}.sqlite3;Version=3;", true);
            connection.Open();
        }

        public DbDataReader SelectReaderFor<TModel>()
            where TModel : IModel {
            var command = new SQLiteCommand(Activator.CreateInstance<TModel>().SelectAllClause, connection);
            return command.ExecuteReader(CommandBehavior.CloseConnection);
        }

        public bool ExecuteClause(string clause, Action<SQLiteCommand> handle) {
            using (var tr = connection.BeginTransaction()) {
                var command = new SQLiteCommand(clause, connection);
                handle(command);
                var result = command.ExecuteNonQuery() > 0;
                if (result) {
                    tr.Commit();
                } else {
                    tr.Rollback();
                }
                return result;
            }
        }

        public long ExecuteIdentity() {
            var command = new SQLiteCommand("SELECT last_insert_rowid();", connection);
            return command.ExecuteScalar().To<long>();
        }

        public ConnectionState State {
            get {
                return connection.State;
            }
        }

        public void Dispose() {
            connection.Close();
        }

    }
}
