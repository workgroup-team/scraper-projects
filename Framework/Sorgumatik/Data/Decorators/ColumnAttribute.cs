﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Data.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class ColumnAttribute : Attribute {

        public ColumnAttribute(string columnName) {
            this.ColumnName = columnName;
        }

        public string ColumnName { get; private set; }
    }
}
