﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Browsers {
    public class SiteInfo {
        public string SourceToNavigateFirst { get; set; }
        public string SourceToLandingPage { get; set; }
    }
}
