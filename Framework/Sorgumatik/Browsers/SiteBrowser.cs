﻿using Sorgumatik.Core;
using Sorgumatik.Core.Helpers;
using Sorgumatik.Core.Objects;
using Sorgumatik.Data.Engines;
using Sorgumatik.Data.Extensions;
using Sorgumatik.Data.Models;
using Sorgumatik.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Controls;
using Application = System.Windows.Forms.Application;
using System.Windows.Navigation;
using static Sorgumatik.Core.Objects.ScriptObject;
using Timer = System.Timers.Timer;
using mshtml;
using System.IO;

namespace Sorgumatik.Browsers {
    public abstract class SiteBrowser {

        public SiteBrowser(WebBrowser parent, ESite site, bool socket = true) {
            this._IsSocket = socket;
            this._Site = site;
            InitializeVariables(parent, site.GetEnumDescription());
            InitalizeServer();
        }

        #region Abstracts
        protected abstract SiteInfo SiteInfo { get; }
        protected abstract int MedianLevel { get; }
        protected abstract void ExtendSession();
        protected abstract ISocketInput TestInputInstance();
        protected abstract ISocketInput NewInputInstance();
        protected abstract void InitiateCascade(ISocketInput input);
        public abstract void AjaxResponse(ISocketInput input);
        public abstract void IFrameResponse(ISocketInput input);
        public abstract void DocumentResponse(ISocketInput input);
        #endregion

        #region Properties
        public AccountModel Account { get; private set; }
        private int _Level;
        private WebBrowser _Browser;
        private ISocketInput SocketInput;

        public WebBrowser Browser => _Browser;
        private dynamic ActiveXInstance => Browser.GetType()
            .GetProperties(BindingFlags.Instance | BindingFlags.NonPublic).SingleOrDefault(p => p.Name == "ActiveXInstance")
            .GetValue(Browser);

        public ScriptObject ScriptObject { get; private set; }

        public delegate void SiteLogInvokedHandler(SiteBrowser browser, string message);
        public event SiteLogInvokedHandler SiteLogInvoked;

        public delegate void SocketSentHandler(SiteBrowser browser, string message);
        public event SocketSentHandler SocketSent;

        public bool Landed { get; private set; }

        private DateTime IdleBegin { get; set; }
        private Timer SessionTicker { get; set; }

        private TcpListener Listener;
        private SocketClient Client;

        private IPEndPoint EndPoint { get; set; }
        protected Stack<string> OutputStack = new Stack<string>();
        private ESite _Site;
        private LicenseResponse License;

        const int AJAX_DELAY = 2000; // non-deterministic wait for AJAX dynamic code
        const int AJAX_DELAY_STEP = 500;
        private readonly bool _IsSocket;

        private bool Listening { get; set; } = false;
        #endregion

        #region Private Methods  
        private void InitializeVariables(WebBrowser parent, string bankName) {
            Account = DataEngine.ReadAll<AccountModel>().SingleOrDefault(acc => acc.Banka == bankName);
            _Browser = parent;
            ScriptObject = new ScriptObject();
            if (_IsSocket) {
                IPHostEntry localhost = Dns.GetHostEntry(Dns.GetHostName());
                var ip = localhost.AddressList.FirstOrDefault(ipa => ipa.AddressFamily == AddressFamily.InterNetwork);
                EndPoint = new IPEndPoint(ip, ValuesEngine.SettingByKey("Port", Account.ID).Value.To<int>());
                Client = new SocketClient(ip, Account);
            }
        }
        private void InitalizeServer() {
            RegistryHelper.FixBrowserVersion();
            RegistryHelper.FixJsonCapture();
            _Browser.HideScriptErrors(true);
            _Browser.ObjectForScripting = ScriptObject;
            ScriptObject.ConsoleLogInvoked -= ScriptObject_ConsoleLogInvoked;
            ScriptObject.ConsoleLogInvoked += ScriptObject_ConsoleLogInvoked;
        }

        private void InitializeTicker() {
            IdleBegin = DateTime.Now;
            var duration = TimeSpan.FromMinutes(1).TotalMilliseconds;
            SessionTicker = new Timer(duration);
            SessionTicker.Elapsed +=
                (s, elapsed) => {
                    if (DateTime.Now.Subtract(IdleBegin).TotalSeconds >= 59) {
                        _Browser.Dispatcher.Invoke(() => ExtendSession());
                    }
                };
            SessionTicker.Start();
        }
        private async void StartServer() {
            Listening = true;
            Listener = new TcpListener(EndPoint);
            Listener.Start();
            await Task.Run(async () => {
                while (Listening) {
                    TcpClient client = null;
                    NetworkStream tcpStream = null;
                    try {
                        client = await Listener.AcceptAsync(new CancellationToken());
                        tcpStream = client.GetStream();
                        int i;
                        var bytes = new byte[65536];
                        if ((i = tcpStream.Read(bytes, 0, bytes.Length)) != 0) {
                            var msg = Encoding.ASCII.GetString(bytes, 0, i);
                            SocketInput = NewInputInstance().Deserialize(msg.Replace("\r\n", ""));
                            InitiateCascade(SocketInput);
                            ProcessStack((data) => {
                                tcpStream.Write(data, 0, data.Length);
                                tcpStream.Flush();
                            });
                            client.Close();
                        }
                    } catch (Exception) {
                        tcpStream?.Flush();
                        tcpStream?.Close();
                        client?.Close();
                    }
                }
            });

        }

        private void ProcessStack(Action<byte[]> dataHandler) {
            var to = ValuesEngine.SettingByKey("Timeout", Account.ID).Value.To<int>();
            while (OutputStack.Count == 0 && to > 0) { Thread.Sleep(100); to -= 100; }
            if (OutputStack.Count == 0) {
                SocketSent?.Invoke(this, "zaman_asimi");
                var tmo = "zaman_asimi".DecodeBytes();
                dataHandler(tmo);
            }
            while (OutputStack.Count > 0) {
                var data = OutputStack.Pop().DecodeBytes();
                SocketSent?.Invoke(this, data.EncodeBytes());
                dataHandler(data);
            }
        }

        // wait until webBrowser.Busy == false or timed out
        private async Task<bool> AjaxDelay(CancellationToken ct, int timeout) {
            using (var cts = CancellationTokenSource.CreateLinkedTokenSource(ct)) {
                cts.CancelAfter(timeout);
                while (true) {
                    try {
                        await Task.Delay(AJAX_DELAY_STEP, cts.Token);
                        var busy = (bool)ActiveXInstance.GetType().InvokeMember("Busy", System.Reflection.BindingFlags.GetProperty, null, ActiveXInstance, new object[] { });
                        Application.DoEvents();
                        if (!busy)
                            return true;
                    } catch (OperationCanceledException) {
                        if (cts.IsCancellationRequested && !ct.IsCancellationRequested)
                            return false;
                        throw;
                    }
                }
            }
        }
        private async void _Browser_LoadCompleted(object sender, NavigationEventArgs e) {
            string url = e.Uri.ToString();
            var timedout = await AjaxDelay(new CancellationToken(), 2000);
            if ((url.StartsWith("http://") || url.StartsWith("https://")) == false) {
                AjaxResponse(SocketInput);
            }
            if (e.Uri.AbsolutePath != Browser.Source.AbsolutePath) {
                IFrameResponse(SocketInput);
            } else {
                DocumentResponse(SocketInput);
            }
        }
        private void AttachCompleted(WebBrowser wb = null) {
            var browser = wb ?? _Browser;
            browser.LoadCompleted -= _Browser_LoadCompleted;
            browser.LoadCompleted += _Browser_LoadCompleted;
        }

        private void ScriptObject_ConsoleLogInvoked(string message) {
            SiteLogInvoked?.Invoke(this, message);
        }
        protected TResult InvokeBrowser<TResult>(Func<WebBrowser, TResult> handler) {
            return _Browser.Dispatcher.Invoke(() => handler(Browser));
        }
        protected void InvokeBrowser(Action<WebBrowser> handler) {
            _Browser.Dispatcher.Invoke(() => handler(Browser));
        }
        protected void NavigateBrowserTo(string url, IDictionary<string, string> data, IDictionary<string, string> headers, bool postWithQueryString = false) {
            _Browser.Dispatcher.Invoke(() => _Browser.NavigateToUrl(url, data, headers, postWithQueryString));
        }
        protected void NavigateBrowserTo(string url) {
            _Browser.Dispatcher.Invoke(() => _Browser.Navigate(url));
        }
        protected void SetLevel(int level, int timeout = 0) {
            //var timedOut = await AjaxDelay(new CancellationToken(), 3000);
            if (timeout > 0) {
                Thread.Sleep(timeout);
            }
            _Level = level;
        }
        protected void SetCompleted(int timeout = 0) {
            if (timeout > 0) {
                Thread.Sleep(timeout);
            }
            _Level = MedianLevel;
        }
        protected int GetLevel() {
            return _Level;
        }
        protected void SetLanded() {
            Landed = true;
            _Level = MedianLevel;
        }
        protected DispHTMLDocument GetIFrame(string frameID) {
            var document = (HTMLDocument)InvokeBrowser(b => b.Document);

            IHTMLElementCollection elcol = document.getElementsByTagName("iframe");
            foreach (IHTMLElement _htmlElement in elcol) {
                HTMLFrameElement frmelement = (HTMLFrameElement)_htmlElement;
                DispHTMLDocument doc2 = (DispHTMLDocument)((SHDocVw.IWebBrowser2)frmelement).Document;
                if (doc2.body != null && frmelement.id == frameID) {
                    return doc2;
                }
            }
            return null;
        }
        #endregion

        #region Public Methods
        public void StartNavigation() {
            AttachCompleted();
            _Browser.Source = new Uri(SiteInfo.SourceToNavigateFirst);
            InitializeTicker();
        }
        public string OpenPort(string appName) {
            var title = appName;
            if (_IsSocket) {
                StartServer();
                title = $"{title} {EndPoint.Address.ToString()}:{ValuesEngine.SettingByKey("Port", Account.ID)?.Value.To<long>()} Adresinden Dinliyor...";
            }
            return title;
        }
        public async void StartLoop(LicenseResponse license, int timeout, int count = 1) {
            License = License ?? license;
            Listening = true;
            await Task.Run(() => {
                Func<bool> proceeds = () => Landed && License != null;
                while (Listening && proceeds() == false) {
                    Thread.Sleep(10);
                }
                while (Listening && proceeds() == true) {
                    try {
                        var error = License.data?.error;
                        var url = $"{License.data?.url}?islem=al&adet={count}";
                        var response = InvokeBrowser(b => b.ReadResponseFrom(url, req => { }));
                        using (var str = response.GetResponseStream()) {
                            using (var reader = new StreamReader(str)) {
                                var lines = reader.ReadToEnd().Split(new[] { "<br>" }, StringSplitOptions.RemoveEmptyEntries);
                                foreach (var line in lines) {
                                    SocketInput = NewInputInstance().Deserialize(line);
                                    InitiateCascade(SocketInput);
                                    ProcessStack((data) => {
                                        var query = data.EncodeBytes();
                                        License.data?.url.MakeSimpleRequest(query);
                                    });
                                }
                                if (lines.Count() == 0) {
                                    Thread.Sleep(timeout * 1000);
                                }
                            }
                        }
                    } catch {
                        continue;
                    }
                }
            });
        }
        public LicenseResponse RegisterLicense(string uuid, string mac) {
            var licenceServer = $"http://www.paystore.com.tr/lisans.php?uid={uuid}&mac={mac}";
            var response = InvokeBrowser(b => b.ReadResponseFrom(licenceServer, req => { }));
            using (var str = response.GetResponseStream()) {
                using (var reader = new StreamReader(str)) {
                    this.License = reader.ReadToEnd().DeserializeTo<LicenseResponse>();
                    var error = License.data?.error;
                    return License != null && string.IsNullOrEmpty(error) ? License : null;
                }
            }
        }
        public void SeizeServer() {
            Listener?.Stop();
            Listening = false;
        }
        public void TestClient() {
            Client?.SendToSocket(TestInputInstance());
        }
        #endregion

    }
}
