﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorgumatik.Browsers {

    public enum ESite {

        [Description("GARANTI")]
        Garanti = 1,
        [Description("YAPI")]
        Yapi,
        [Description("ISIMIZFATURA")]
        IsimizFatura,
        [Description("ISBANK")]
        Isbank

    }

}
