﻿using mshtml;
using Sorgumatik.Core.Objects;
using Sorgumatik.Data.Engines;
using Sorgumatik.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using HtmlElement = System.Windows.Forms.HtmlElement;

namespace Sorgumatik.Extensions {

    public static class WebExtensions {

        public static IEnumerable<dynamic> FindElementsByClassName(this WebBrowser browser, string className, string tagName = "input") {
            dynamic document = browser.Document;
            foreach (dynamic input in document.getElementsByTagName(tagName)) {
                if (input.getAttribute("className") == className) {
                    yield return input;
                }
            }
        }

        public static IEnumerable<dynamic> FindElementsBy(this WebBrowser browser, Func<dynamic, bool> selector, string tagName = "input") {
            dynamic document = browser.Document;
            foreach (dynamic input in document.getElementsByTagName(tagName)) {
                if (selector(input) == true) {
                    yield return input;
                }
            }
        }

        public static IHTMLElement FindByIdentity(this WebBrowser browser, string identity, string tagName = "input") {
            var document = browser.Document as HTMLDocument;
            try {
                foreach (IHTMLElement input in document.getElementsByTagName(tagName)) {
                    if (input.getAttribute("name") == identity || input.getAttribute("id") == identity) {
                        return input;
                    }
                }
                return null;
            } catch (Exception) {
                return null;
            }
        }

        public static IEnumerable<dynamic> FindAllBySelector(this WebBrowser browser, string selector) {
            dynamic document = browser.Document;
            foreach (dynamic input in document.querySelectorAll(selector)) {
                yield return input;
            }
        }

        public static dynamic FindBySelector(this WebBrowser browser, string selector) {
            dynamic document = browser.Document;
            try {
                return document.querySelector(selector);
            } catch {
                return null;
            }
        }

        public static dynamic FindFirstByTagName(this WebBrowser browser, string tagName) {
            dynamic document = browser.Document;
            return document.getElementsByTagName(tagName)[0];
        }

        public static dynamic NewElement(this WebBrowser browser, string tagName) {
            dynamic document = browser.Document;
            return document.createElement(tagName);
        }

        public static string GetAttributeOf(this WebBrowser browser, string id, string attributeName, string tagName = "input") {
            dynamic element = browser.FindByIdentity(id);
            return element?.getAttribute(attributeName);
        }

        public static string GetBody(this WebBrowser browser) {
            return ((dynamic)browser.Document).documentElement.InnerHtml;
        }

        public static bool SetValueOfInput(this WebBrowser browser, string id, string value) {
            dynamic input = browser.FindByIdentity(id);
            input?.setAttribute("value", value);
            return input != null;
        }

        public static bool Click(this WebBrowser browser, string identity) {
            var found = false;
            var doc = browser.Document as IHTMLDocument2;
            foreach (var item in doc.all.tags("input")) {
                if (item.id == identity) {
                    item.click();
                    found = true;
                    break;
                }
            }
            foreach (var item in doc.all.tags("button")) {
                if (item.id == identity) {
                    item.click();
                    found = true;
                    break;
                }
            }
            if (found == false) {
                dynamic input = browser.FindByIdentity(identity, "button");
                if (input == null) {
                    input = browser.FindByIdentity(identity);
                }
                input?.click();
                found = input != null;
            }
            return found;
        }
        public static bool ClickToSelector(this WebBrowser browser, string selector) {
            var input = browser.FindBySelector(selector);
            input?.click();
            return input != null;
        }

        public static bool SubmitForm(this WebBrowser browser, string id) {
            var input = browser.FindByIdentity(id, "form");
            if (input == null) {
                input = browser.FindByIdentity(id);
            }
            input?.GetType().InvokeMember("submit", BindingFlags.InvokeMethod | BindingFlags.NonPublic, null, input, new object[] { });
            return input != null;
        }

        public static bool HideScriptErrors(this WebBrowser wb, bool hide) {
            var fiComWebBrowser = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return false;
            var objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null) {
                wb.Loaded += (o, s) => wb.HideScriptErrors(hide); //In case we are to early
                return false;
            }
            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { hide });
            return true;
        }
        public static void ExecScript(this WebBrowser wb, string funcName, string script, int timeout = 0) {
            wb._ExecScript(funcName, script, false, timeout);
        }
        public static string ExecScriptResult(this WebBrowser wb, string funcName, string script) {
            return wb._ExecScript(funcName, script, true, 0);
        }
        private static string _ExecScript(this WebBrowser wb, string funcName, string script, bool returnResult, int timeout) {
            var returnStatement = returnResult ? "return " : "";
            script = script.Trim().TrimEnd(';');
            try {
                var call = $"function(){{ var r = {script}; external.log('{funcName} > {script.ToEscaped()} > ' + r); {returnStatement}r; }}";
                if (timeout > 0) {
                    call = $"setTimeout({call}, {timeout});";
                } else {
                    call = $"({call})();";
                }
                var result = wb.InvokeScript("eval", new[] { call })?
                .ToString();
                return result;
            } catch {
                return string.Empty;
            }

        }

        public static void NavigateToUrl(this WebBrowser browser, string url, IDictionary<string, string> data, IDictionary<string, string> headers, bool postWithQueryString = false) {
            var bytes = data.ToHttpPostParameterBytes();
            headers.Add(new KeyValuePair<string, string>("Content-Type", "application/x-www-form-urlencoded"));
            headers.Add(new KeyValuePair<string, string>("Content-Length", bytes.Length.ToString()));
            var modifiedUrl = url;
            if (postWithQueryString) {
                modifiedUrl = $"{url}?{data.ToHttpPostParameterString()}";
            }
            browser.Navigate(modifiedUrl, null, bytes, headers.ToHttpHeaderString());
        }

        public static void NavigateSettingCookie(this WebBrowser wb, string url, IDictionary<string, string> headers, IEnumerable<(string name, string value)> cookies) {
            (wb.Document as HTMLDocument).cookie +=
                cookies.Aggregate("; ", (acc, ck) => acc + $"{ck.name}={ck.value}; ").StripTrailing("; ");
            wb.Navigate(url, null, new byte[] { }, headers.ToHttpHeaderString());
        }

        public static WebResponse ReadResponseFrom(this WebBrowser browser, string url, Action<WebRequest> invoker) {
            var request = WebRequest.Create(url) as HttpWebRequest;
            request.CookieContainer = new CookieContainer();
            request.CookieContainer.SetCookies(new Uri(url), (browser.Document as HTMLDocument)?.cookie ?? "");
            request.AllowAutoRedirect = false;
            invoker(request);
            var response = request.GetResponse();
            if (browser.Document != null) {
                (browser.Document as HTMLDocument).cookie += request.CookieContainer.GetCookies(new Uri(url)).OfType<Cookie>().Aggregate("; ", (acc, ck) => $"{ck.Name}={ck.Value}; ").StripTrailing("; "); 
            }
            return response;
        }

        public static WebResponse MakeSimpleRequest(this string url, string queryString = "") {
            var request = WebRequest.Create(queryString == "" ? url : $"{url}?{queryString}") as HttpWebRequest;
            var response = request.GetResponse();
            return response;
        }

        public static string Body(this WebBrowser wb, int stage, out string content) {
            var key = stage.ToString();
            string body = ((dynamic)wb.Document).documentElement.InnerHtml;
            body = body.ToSafeJson(out var arrayStr).Replace("<head></head><body>", "").Replace("</body>", "");
            content = arrayStr;
            return body;
        }

        public static string ToHttpHeaderString(this IDictionary<string, string> dict) {
            return dict.Aggregate("", (str, n) => $@"{n.Key}: {n.Value}{Environment.NewLine}{str}").Trim();
        }

        public static byte[] ToHttpPostParameterBytes(this IDictionary<string, string> data) {
            return Encoding.ASCII.GetBytes(data.Aggregate("", (str, n) => $@"{n.Key}={WebUtility.UrlEncode(n.Value)}&{str}").TrimEnd('&'));
        }

        public static string ToHttpPostParameterString(this IDictionary<string, string> data) {
            return data.Aggregate("", (str, n) => $@"{n.Key}={WebUtility.UrlEncode(n.Value)}&{str}").TrimEnd('&');
        }

        private static string ToSafeJson(this string self, out string arrayStr) {
            var match = self.Matches(@"""content"":\s+""(.*)"",\s+""comesFromLayer").FirstOrDefault()?.Groups[1].Value ?? "";
            arrayStr = match.Replace(@"content"": "" ", "").Replace(@""",\n\s\s""comesFromLayer", "");
            if (string.IsNullOrEmpty(arrayStr) == false) {
                self = self.Replace(arrayStr, "");
                arrayStr = arrayStr.Trim().Replace("\\r\\n", "").Replace("\\n", "")
                    .Replace("\\\\", "\\")
                    .Replace("\\\"", "'")
                    .Replace("''", "'")
                    .Replace("false", @"""false""").Replace("true", @"""true""");
            }
            return self;
        }

        public static string InfoBean(this DateTime date) {

            var tk = "±";
            string cInfoBean = $"1536x864{tk}5{tk}H{tk}{DateTime.Now.Day.Digitalize() + '.' + (DateTime.Now.Month + 1).Digitalize() + '.' + DateTime.Now.Year}{tk}{DateTime.Now.Hour.Digitalize() + '.' + DateTime.Now.Minute.Digitalize() + '.' + DateTime.Now.Second.Digitalize()}{tk}{date.ToString("ddd, dd MMM yyyy HH:mm:ss")} GMT{tk}Y";

            return cInfoBean;

        }
        public static string Digitalize(this int f) {

            string Sonuc = "";
            if (f < 0 || f > 9) {

                Sonuc = f.ToString();

            } else {
                Sonuc = "0" + f.ToString();
            }

            return Sonuc;

        }

    }

}
