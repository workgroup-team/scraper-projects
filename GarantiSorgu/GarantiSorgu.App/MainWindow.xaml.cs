﻿using GarantiSorgu.App.Client;
using GarantiSorgu.App.Code;
using GarantiSorgu.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GarantiSorgu.App {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
            InitializeServer();
            MessageBox.Show("Lütfen Navigasyon Menüsünüden Hedef Seçiniz");
        }

        #region Private Methods
        private void InitializeServer() {
            Server?.Seize();
            InitializeBrowser();
            InitializeConfigurations();
            Server = Server ?? new Channel();
            Client = Client ?? new SocketClient();
            this.Title = $"Garanti Sorgumatik Sunucusu Kapalı..";
            SwitchMenuItems(on: false);
        }

        private void OpenPort() {
            Server.StartListening();
            Server.ListenAsync(async (input, listener) => {
                if (SessionEngine.Loggedin) {
                    await GarantiWebBrowser.Dispatcher.InvokeAsync(() => GarantiWebBrowser.PostSession(input, listener));
                }
            });
            Server.SocketHandled -= Server_SocketHandled;
            Server.SocketHandled += Server_SocketHandled;

            this.Title = $"Garanti Sorgumatik {IPAddress.Loopback.ToString()}:{SettingsEngine.SettingByKey("Port")?.Value.To<long>()} Adresinden Dinliyor...";
            SwitchMenuItems(on: true);
        }

        private void SwitchMenuItems(bool on) {
            SocketServerAcceptMenuItem.IsEnabled = !on;
            SocketServerSeizeMenuItem.IsEnabled = on;
            ChannelOpenPortMenuItem.IsEnabled = !on;
            ChannelSeizePortMenuItem.IsEnabled = on;
        }

        private void InitializeConfigurations() {
            System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(this);
        }

        private void InitializeBrowser() {
            RegistryHelper.FixBrowserVersion();
            RegistryHelper.FixJsonCapture();
            GarantiWebBrowser.HideScriptErrors(true);
            GarantiWebBrowser.ObjectForScripting = SessionEngine.ScriptObject;
            SessionEngine.ScriptObject.ConsoleLogInvoked -= ScriptObject_ConsoleLogInvoked;
            SessionEngine.ScriptObject.ConsoleLogInvoked += ScriptObject_ConsoleLogInvoked;

            InitializeTicker();
        }

        private void InitializeTicker() {
            IdleBegin = DateTime.Now;
            var duration = TimeSpan.FromMinutes(1).TotalMilliseconds;
            SessionTicker = new Timer(duration);
            SessionTicker.Elapsed +=
                (s, elapsed) => {
                    if (DateTime.Now.Subtract(IdleBegin).TotalSeconds >= 59) {
                        GarantiWebBrowser.Dispatcher.Invoke(() => GarantiWebBrowser.ExtendSession());
                    }
                };
            SessionTicker.Start();
        }

        private void SeizeServer() {
            Client?.End();
            SessionTicker?.Stop();
            Server?.Seize();
            this.Title = $"Garanti Sorgumatik Sunucusu Kapalı..";
            SwitchMenuItems(on: false);
        }

        #endregion

        private Timer SessionTicker { get; set; }
        private Channel Server { get; set; }
        private SocketClient Client { get; set; }
        private DateTime IdleBegin { get; set; }

        #region Handlers
        private void ScriptObject_ConsoleLogInvoked(string message) {
            txtConsole.Text = $"{message}\n{new String('*', 14)}\n" + txtConsole.Text;
        }
        private void Server_SocketHandled(SocketOutput output) {
            txtConsole.Text = $"{output.Serialize()}\n{new String('*', 66)}\n" + txtConsole.Text;
            IdleBegin = DateTime.Now;
        }
        private void Window_Closed(object sender, EventArgs e) {
            SeizeServer();
        }
        private void GarantiWebBrowser_LoadCompleted(object sender, NavigationEventArgs e) {
            if (sender is WebBrowser browser) {
                if (SessionEngine.Loggedin == false) {
                    browser.Login("IsmailGencan");
                }
            }
        }
        private void DbSettingsMenuItem_Click(object sender, RoutedEventArgs e) {
            var dbWindow = new DBSettings();
            dbWindow.ShowDialog();
        }
        private void ExitAppMenuItem_Click(object sender, RoutedEventArgs e) {
            if (MessageBox.Show("Çıkış Yapılıyor..", "Tasdik", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation) == MessageBoxResult.OK) {
                this.Close();
            }
        }
        private void SocketServerAcceptMenuItem_Click(object sender, RoutedEventArgs e) {
            if (MessageBox.Show("Sunucu Resetlenecek..", "Tasdik", MessageBoxButton.OKCancel, MessageBoxImage.Exclamation) == MessageBoxResult.OK) {
                SeizeServer();
                OpenPort();
                InitializeServer();
            }
        }
        private void SocketServerSeizeMenuItem_Click(object sender, RoutedEventArgs e) {
            SeizeServer();
        }
        private void btnSendToSocket_Click(object sender, RoutedEventArgs e) {
            foreach (var dict in txtCorporateID.Text.Split(',').Zip(txtSearchItem.Text.Split(','), (f, s) => (id: f, se: s))) {
                var input = new SocketInput { CorporationID = dict.id.To<long>(), SearchItem = dict.se, SearchItem2 = txtSearchItem2.Text };
                Client.SendToSocket(input);
            }
        }
        private void ChannelOpenPortMenuItem_Click(object sender, RoutedEventArgs e) {
            OpenPort();
        }

        private void ChannelSeizePortMenuItem_Click(object sender, RoutedEventArgs e) {
            SeizeServer();
        }
        #endregion

        private void BrowsingEndUserMenuItem_Click(object sender, RoutedEventArgs e) {
            GarantiWebBrowser.Navigate(SettingsEngine.SettingByKey("LoginUrl")?.Value);
        }

        private void BrowsingCorporateMenuItem_Click(object sender, RoutedEventArgs e) {
            GarantiWebBrowser.Navigate(SettingsEngine.SettingByKey("LoginCorporateUrl")?.Value);
        }
    }
}
