﻿using GarantiSorgu.App.Code;
using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GarantiSorgu.App {
    /// <summary>
    /// Interaction logic for DBSettings.xaml
    /// </summary>
    public partial class DBSettings : Window {
        public DBSettings() {
            InitializeComponent();
            PopulateData();
        }

        private void PopulateData() {
            txtPort.Text = SettingsEngine.SettingByKey("Port").Value;
            txtTimeout.Text = SettingsEngine.SettingByKey("Timeout").Value;
            dgrCorporates.ItemsSource = (from c in DataManager.ReadAll<CorporateModel>() select new {
                KurumID = c.KurumID,
                KurumAdi = c.KurumAdi,
                BransAdi = c.BransAdi,
                KurumIndeksi = c.Index,
                FaturaTipIndeksi = c.FaturaTipIndex
            }).ToList();
            SettingsEngine.Refresh();
        }

        private CorporateModel _SelectedCorporate = null;
        private void ChangeSetting<TSetting>(string key, TSetting value) {
            if (value.Equals(default(TSetting))) {
                MessageBox.Show(this, $"{key} {default(TSetting)}'a Ayarlanamaz.",
                    "Hata", MessageBoxButton.OK, MessageBoxImage.Error);
            } else {
                var setting = SettingsEngine.SettingByKey(key);
                setting.Value = value.ToString();
                try {
                    setting.UpsertSelf(s => s.SettingID);
                    MessageBox.Show(this, $"{key} Kaydı Başarılı.",
                        "İşlem başarılı", MessageBoxButton.OK, MessageBoxImage.Information);
                    PopulateData();
                } catch (Exception exc) {
                    MessageBox.Show(this, $"{key} Kaydı Başarısız.\n{exc.Message}",
                        "Hata Oluştu", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        #region Handlers
        private void btnPort_Click(object sender, RoutedEventArgs e) {
            ChangeSetting("Port", txtPort.Text.To<int>());
        }

        private void btnTimeout_Click(object sender, RoutedEventArgs e) {
            ChangeSetting("Timeout", txtTimeout.Text.To<int>());
        }
        private void dgrCorporates_SelectionChanged(object sender, SelectionChangedEventArgs e) {
            dynamic item = (sender as DataGrid).SelectedItem;
            if (item != null) {
                long kurumid = item.KurumID;
                _SelectedCorporate = DataManager.ReadAll<CorporateModel>().SingleOrDefault(c => c.KurumID == kurumid);
                txtCorporateName.Text = _SelectedCorporate.KurumAdi;
                txtBranchName.Text = _SelectedCorporate.BransAdi;
                txtCorporateIndex.Text = _SelectedCorporate.Index.ToString();
                txtInvoiceTypeIndex.Text = _SelectedCorporate.FaturaTipIndex.ToString();
            }
        }
        private void btnUpdateCorporation_Click(object sender, RoutedEventArgs e) {
            if (_SelectedCorporate == null) {
                MessageBox.Show(this, $"Lütfen Bir Kurum Seçiniz",
                    "Hata", MessageBoxButton.OK, MessageBoxImage.Error);
            } else {
                _SelectedCorporate.KurumAdi = txtCorporateName.Text;
                _SelectedCorporate.BransAdi = txtBranchName.Text;
                _SelectedCorporate.Index = txtCorporateIndex.Text.To<long>();
                _SelectedCorporate.FaturaTipIndex = txtInvoiceTypeIndex.Text.To<long>();
                _SelectedCorporate.UpsertSelf(c => c.KurumID);
                PopulateData();
            }
        }
        private void btnInsertCorporation_Click(object sender, RoutedEventArgs e) {
            var corporation = new CorporateModel();
            corporation.KurumAdi = txtCorporateName.Text;
            corporation.BransAdi = txtBranchName.Text;
            corporation.Index = txtCorporateIndex.Text.To<long>();
            corporation.FaturaTipIndex = txtInvoiceTypeIndex.Text.To<long>();
            var inserted = corporation.UpsertSelf(c => c.KurumID);
            if (inserted.KurumID > 0) {
                MessageBox.Show(this, $"{inserted.KurumAdi} Kurumu Kaydedildi",
                    "İşlem", MessageBoxButton.OK, MessageBoxImage.Information);
                PopulateData();
            } else {
                MessageBox.Show(this, $"Kurum Kaydedilemedi",
                   "Hata", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        #endregion

    }
}
