﻿
using System;
using System.ComponentModel;
using GarantiSorgu.Core.Data;
using System.Text;
using System.Linq;
using System.Threading;
using System.Windows.Controls;
using GarantiSorgu.App.Decorators;
using System.Reflection;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using System.Collections.Generic;

namespace GarantiSorgu.App.Code {

    public class SocketOutput {

        public SocketOutput(string errorText) {
            this._ErrorText = errorText;
        }

        public SocketOutput(ResponseObject obj, PostParams pparams) {
            if (pparams.Finalized == true) {
                _ResponseObject = obj;
                _SocketInput = pparams.Input;
                Finalized = true;
                var content = obj?.content.DecodeString();
                foreach (var prop in this.GetType().PublicPropertiesOf().Where(p => p.HasAttribute<PatternLocatorAttribute>())) {
                    prop.SetValue(this, null);
                }
                foreach (var prop in this.GetType().PublicPropertiesOf().Where(p => p.HasAttribute<PatternLocatorAttribute>())) {
                    var plocator = prop.GetCustomAttribute<PatternLocatorAttribute>();
                    var pattern = SettingsEngine.SettingByKey(plocator.Locator)?.Value;
                    if (plocator.Locator == "SocketCustomerName") {
                        if (prop.GetValue(this) == null) {
                            prop.SetValue(this, content?.Matches(pattern).AtIndexOrDefault(4)?.Groups[1].Value);
                        }
                    } else {
                        var matches = content?.Matches(pattern);
                        var arr = new string[matches.Count()];
                        foreach (var match in matches.Select((value, i) => new { i, value })) {
                            arr[match.i] = match.value?.Groups[1].Value;
                        }
                        prop.SetValue(this, arr);
                    }
                }
            }
        }

        private ResponseObject _ResponseObject;
        private SocketInput _SocketInput;
        private string _ErrorText;

        public bool Finalized { get; internal set; } = false;

        [PatternLocator("SocketCustomerName", -1)]
        [Description("abone_adi")]
        public string CustomerName { get; set; }
        [PatternLocator("SocketInvoiceNumber", -1)]
        [Description("fatura_no")]
        public string[] InvoiceNumbers { get; set; }
        [PatternLocator("SocketAmount", -1)]
        [Description("fatura_tutari")]
        public string[] Amounts { get; set; }
        [PatternLocator("SocketLastDueDate", -1)]
        [Description("son_odeme")]
        public string[] LastDueDates { get; set; }

        public string Serialize() {
            var root = new XElement("faturaSorguSonuc");
            if (_ErrorText.IsAssigned()) {
                root.Add(new XElement("hata", _ErrorText));
            } else {
                var invoiceNumbers = new List<XElement>();
                var amounts = new List<XElement>();
                var lastDueDates = new List<XElement>();
                object abone_adi = null;
                var count = new int[] { InvoiceNumbers.Count(), Amounts.Count(), LastDueDates.Count() }.Max();
                foreach (var prop in this.GetType().PublicPropertiesOf()
                    .Where(p => p.HasAttribute<PatternLocatorAttribute>())) {
                    var descriptor = prop.GetCustomAttribute<DescriptionAttribute>().Description;
                    if (descriptor == "abone_adi") {
                        abone_adi = prop.GetValue(this);
                        if (abone_adi.IsAssigned() == true) {
                            root.Add(new XElement(descriptor, abone_adi));
                        } else {
                            break;
                        }
                    } else {
                        List<XElement> elements = null;
                        if (descriptor == "fatura_no") {
                            elements = invoiceNumbers;
                        } else if (descriptor == "fatura_tutari") {
                            elements = amounts;
                        } else if (descriptor == "son_odeme") {
                            elements = lastDueDates;
                        }
                        var arrstr = prop.GetValue(this) as string[];
                        for (int i = 0; i < count; i++) {
                            elements.Add(new XElement(descriptor, arrstr.AtIndexOrDefault(i)));
                        }
                    }
                }
                var faturalar = new XElement("faturalar");
                for (int i = 0; i < count; i++) {
                    var fatura = new XElement("fatura");
                    fatura.Add(invoiceNumbers.AtIndexOrDefault(i));
                    fatura.Add(amounts.AtIndexOrDefault(i));
                    fatura.Add(lastDueDates.AtIndexOrDefault(i));
                    faturalar.Add(fatura);
                }

                if (count > 0) {
                    root.Add(new XElement("tesisat", _SocketInput.SearchItem));
                    root.Add(new XElement("ekalan", _SocketInput.SearchItem2));
                    root.Add(new XElement("faturaAdet", Amounts.Count()));
                    root.Add(faturalar);
                } else if (abone_adi.IsAssigned() == false) {
                    var msg = _ResponseObject?.message.DecodeString().HtmlDecode();
                    var hata = msg.Matches(@"<span>(.*?)<\/span>").FirstOrDefault()?.Groups[1].Value;
                    root.Add(new XElement("hata", hata));
                } else {
                    root.Add(new XElement("hata", "Ödenecek fatura bilgisi bulunamadı"));
                }
            }
            var doc = new XDocument(root);
            using (var stringWriter = new StringWriter()) {
                using (var xmlTextWriter = XmlWriter.Create(stringWriter)) {
                    doc.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    return stringWriter.GetStringBuilder().ToString();
                }
            }
        }
    }

}