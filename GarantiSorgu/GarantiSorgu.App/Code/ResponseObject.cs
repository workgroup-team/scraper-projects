﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace GarantiSorgu.App.Code {
    public class ResponseObject {

        public string retc { get; set; }
        public string chart { get; set; }
        public bool isWarning { get; set; }
        public string message { get; set; }
        public bool nextPage { get; set; }
        public bool isSSVErr { get; set; }
        public string redirect { get; set; }
        public string contentType { get; set; }
        [ScriptIgnore]
        public string content { get; private set; }
        public bool  comesFromLayer { get; set; }
        public bool isSeperateAjaxErrorPage { get; set; }
        public bool errorPartialDisplayOk { get; set; }

        private bool cSet = false; 
        public void SetContent(string c) {
            if (cSet == false) {
                content = c;
                cSet = true;
            }
        }

    }
}
