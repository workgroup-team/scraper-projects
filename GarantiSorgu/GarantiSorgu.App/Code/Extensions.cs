﻿using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Models;
using mshtml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using HtmlElement = System.Windows.Forms.HtmlElement;

namespace GarantiSorgu.App.Code {
    public static class Extensions {

        public static dynamic FindElementByIdentity(this WebBrowser browser, string identity, string tagName = "input") {
            dynamic document = browser.Document;
            try {
                foreach (dynamic input in document.getElementsByTagName(tagName)) {
                    if (input.getAttribute("name") == identity || input.getAttribute("id") == identity) {
                        return input;
                    }
                }
                return null;
            } catch (Exception) {
                return null;
            }
        }

        public static dynamic FindFirstElementByTagName(this WebBrowser browser, string tagName) {
            dynamic document = browser.Document;
            return document.getElementsByTagName(tagName)[0];
        }

        public static dynamic NewElement(this WebBrowser browser, string tagName) {
            dynamic document = browser.Document;
            return document.createElement(tagName);
        }

        public static string GetAttributeOf(this WebBrowser browser, string id, string attributeName, string tagName = "input") {
            var document = browser.Document as IHTMLDocument3;
            dynamic element = browser.FindElementByIdentity(id);
            return element?.getAttribute(attributeName);
        }

        public static bool SetValueOfInput(this WebBrowser browser, string id, string value) {
            dynamic document = browser.Document;
            dynamic input = browser.FindElementByIdentity(id);
            input?.setAttribute("value", value);
            return input != null;
        }

        public static bool Click(this WebBrowser browser, string id) {
            var document = browser.Document as IHTMLDocument3;
            dynamic input = browser.FindElementByIdentity(id, "button");
            if (input == null) {
                input = browser.FindElementByIdentity(id);
            }
            input?.click();
            return input != null;
        }

        public static bool HideScriptErrors(this WebBrowser wb, bool hide) {
            var fiComWebBrowser = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return false;
            var objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null) {
                wb.Loaded += (o, s) => wb.HideScriptErrors(hide); //In case we are to early
                return false;
            }
            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { hide });
            return true;
        }
        public static void ExecScript(this WebBrowser wb, string funcName, string script, int timeout = 0) {
            wb._ExecScript(funcName, script, false, timeout);
        }
        public static string ExecScriptResult(this WebBrowser wb, string funcName, string script) {
            return wb._ExecScript(funcName, script, true, 0);
        }
        private static string _ExecScript(this WebBrowser wb, string funcName, string script, bool returnResult, int timeout) {
            var returnStatement = returnResult ? "return " : "";
            script = script.Trim().TrimEnd(';');
            try {
                var call = $"function(){{ var r = {script}; external.log('{funcName} > {script.ToEscaped()} > ' + r); {returnStatement}r; }}";
                if (timeout > 0) {
                    call = $"setTimeout({call}, {timeout});";
                } else {
                    call = $"({call})();";
                }
                var result = wb.InvokeScript("eval", new[] { call })?
                .ToString();
                return result;
            } catch {
                return string.Empty;
            }

        }

        public static void NavigateToParams(this WebBrowser wb, PostParams pparams, bool useNetHttp = false) {
            var url = pparams.ParseSubmitUrl();
            if (useNetHttp == false) {
                var headers = pparams.ParseHeaders(out var bytes);
                wb.Navigate(url, null, bytes, headers);
            } else {
                var request = WebRequest.Create(url) as HttpWebRequest;
                request.CookieContainer = new CookieContainer();
                request.CookieContainer.SetCookies(new Uri(url), (wb.Document as HTMLDocument)?.cookie);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.GetResponse();
            }
        }

        public static string Body(this WebBrowser wb, int stage, out string content) {
            var key = stage.ToString();
            string body = ((dynamic)wb.Document).documentElement.InnerHtml;
            body = body.ToSafeJson(out var arrayStr).Replace("<head></head><body>", "").Replace("</body>", "");
            content = arrayStr;
            return body;
        }

        private static string ToSafeJson(this string self, out string arrayStr) {
            var match = self.Matches(@"""content"":\s+""(.*)"",\s+""comesFromLayer").FirstOrDefault()?.Groups[1].Value ?? "";
            arrayStr = match.Replace(@"content"": "" ", "").Replace(@""",\n\s\s""comesFromLayer", "");
            if (string.IsNullOrEmpty(arrayStr) == false) {
                self = self.Replace(arrayStr, "");
                arrayStr = arrayStr.Trim().Replace("\\r\\n", "").Replace("\\n", "")
                    .Replace("\\\\", "\\")
                    .Replace("\\\"", "'")
                    .Replace("''", "'")
                    .Replace("false", @"""false""").Replace("true", @"""true""");
            }
            return self;
        }

        public static int LoginStage(this Uri url) {
            if (url.ToString().StartsWith(SettingsEngine.SettingByKey("LoginUrl")?.Value)) {
                return 1;
            } else if (url.ToString().StartsWith(SettingsEngine.SettingByKey("LoginCorporateUrl")?.Value)) {
                return 1;
            } else if (url.ToString().StartsWith(SettingsEngine.SettingByKey("SecurityUrl")?.Value)) {
                return 2;
            } else if (url.ToString().StartsWith(SettingsEngine.SettingByKey("SignedinUrl")?.Value)) {
                return 3;
            }
            return 0;
        }
    }
}
