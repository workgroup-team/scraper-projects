﻿using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GarantiSorgu.App.Code {
    public static class SessionEngine {

        public static bool Loggedin { get; private set; } = false;
        public static ScriptObject ScriptObject { get; internal set; } = new ScriptObject();
        public static PostParams PParams { get; internal set; } = new PostParams();

        public static void Login(this WebBrowser browser, string accountName) {
            var stage = browser.Source.LoginStage();
            var hesap = DataManager.ReadAll<AccountModel>().SingleOrDefault(h => h.HesapAdi == accountName);
            if (stage == 1) {
                browser.SetValueOfInput("custno", hesap.HesapNo);
                browser.SetValueOfInput("password", hesap.Sifre);
                browser.Click("formSubmit");
            } else if (stage == 2) {
                browser.SetValueOfInput("custno", hesap.HesapNo);
                browser.SetValueOfInput("password", hesap.Sifre);
                browser.Click("formSubmit");
            } else if (stage == 3) {
                Loggedin = true;
            }
        }

        public static void ExtendSession(this WebBrowser browser) {
            browser.ExecScript("DisableCancelDialog", "$('form#command #cancel').click()");
        }

        public static void PostSession(this WebBrowser browser, SocketInput input, Action<SocketOutput> outputHandler) {
            var corporate = DataManager.ReadAll<CorporateModel>()
                .FirstOrDefault(c => c.KurumID == input?.CorporationID);
            var RequestObject = new RequestObject();
            RequestObject.LoadVars(browser, PParams);
            RequestObject.ReadPParams(browser, corporate, input, DateTime.Now.InfoBean(), 3);
            RequestObject.OutputParsed += (requestObject, output) => {
                if (output.Finalized == false) {
                    requestObject.PostCascade();
                } else {
                    outputHandler(output);
                }
            };
            RequestObject.PostCascade();
        }

        private static string InfoBean(this DateTime date) {

            var tk = "±";
            string cInfoBean = $"1536x864{tk}5{tk}H{tk}{DateTime.Now.Day.Digitalize() + '.' + (DateTime.Now.Month + 1).Digitalize() + '.' + DateTime.Now.Year}{tk}{DateTime.Now.Hour.Digitalize() + '.' + DateTime.Now.Minute.Digitalize() + '.' + DateTime.Now.Second.Digitalize()}{tk}{date.ToString("ddd, dd MMM yyyy HH:mm:ss")} GMT{tk}Y";

            return cInfoBean;

        }
        private static string Digitalize(this int f) {

            string Sonuc = "";
            if (f < 0 || f > 9) {

                Sonuc = f.ToString();

            } else {
                Sonuc = "0" + f.ToString();
            }


            return Sonuc;


        }

    }
}
