﻿
using GarantiSorgu.Core.Data;
using System;
using System.Linq;

namespace GarantiSorgu.App.Code {

    public class SocketInput {
        public long CorporationID { get; set; }
        public string SearchItem { get; set; }
        public string SearchItem2 { get; set; }

        internal static SocketInput Deserialize(string data) {
            var parts = data.Split('&');
            if (parts.Length >= 2) {
                return new SocketInput {
                    CorporationID = parts.SingleOrDefault(p => p.Split('=').FirstOrDefault() == "kurum_id")?
                    .Split('=').AtIndexOrDefault(1)?.To<long>() ?? 0,
                    SearchItem = parts.SingleOrDefault(p => p.Split('=').FirstOrDefault() == "alan_1")?
                    .Split('=').AtIndexOrDefault(1),
                    SearchItem2 = parts.SingleOrDefault(p => p.Split('=').FirstOrDefault() == "alan_2")?
                    .Split('=').AtIndexOrDefault(1)
                };
            } else {
                return new SocketInput();
            }
        }

        internal string Serialize() {
            return $"kurum_id={CorporationID}&alan_1={SearchItem}&alan_2={SearchItem2}";
        }
    }

}