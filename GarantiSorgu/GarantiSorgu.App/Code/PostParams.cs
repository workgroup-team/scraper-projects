﻿using GarantiSorgu.App.Decorators;
using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GarantiSorgu.App.Code {
    public class PostParams {

        [ScriptLocator("ParamsPaymentUrlScript")]
        public string PaymentUrl { get; set; }
        [ScriptLocator("ParamsCsrfScript")]
        public string CSRF { get; set; }

        [PatternLocator("ParamsSuggestDataUrl", 3, DoNotFlush = false)]
        public string SuggestDataUrl { get; set; }
        [PatternLocator("ParamsSuggestRequestUrl", 3, DoNotFlush = false)]
        public string SuggestRequestUrl { get; set; }

        [PatternLocator("ParamsSubmitUrl", 3)]
        public string SubmitUrl { get; set; }

        [PatternLocator("ParamsSearchLinkPattern", 4)]
        public string SearchLink { get; set; }
        [PatternLocator("ParamsInvoiceType", 5)]
        public string InvoiceType { get; set; }
        [PatternLocator("ParamsSubInvoiceType", 5)]
        public string SubInvoiceType { get; set; }
        [PatternLocator("ParamsCorporateType", 5)]
        public string CorporateType { get; set; }
        [PatternLocator("ParamsSearchText", 5)]
        public string SearchText { get; set; }

        public CorporateModel Corporate { get; private set; }
        public SocketInput Input { get; private set; }
        public string Bean { get; private set; }
        public bool Finalized { get => this._CurrentStage == 7; }
        public int CurrentStage { get => _CurrentStage; }

        private int _CurrentStage;
        private ResponseObject _CurrentResponseObject;

        public void ReadContent(WebBrowser browser, CorporateModel corporate, SocketInput input, string bean, int currentStage) {
            this.Corporate = corporate;
            this.Input = input;
            this.Bean = bean;
            this._CurrentStage = currentStage;
            foreach (var prop in this.GetType().PublicPropertiesOf().Where(p => p.HasAttribute<ScriptLocatorAttribute>())) {
                var locator = prop.GetCustomAttribute<ScriptLocatorAttribute>().Locator;
                var script = SettingsEngine.SettingByKey(locator)?.Value;
                var result = browser.ExecScriptResult(locator, script);
                if (string.IsNullOrEmpty(result) == false) {
                    prop.SetValue(this, result);
                }
            }
            foreach (var prop in this.GetType().PublicPropertiesOf().Where(p => p.HasAttribute<ContentLocatorAttribute>())) {
                var attr = prop.GetCustomAttribute<ContentLocatorAttribute>();
                var content = SettingsEngine.SettingByKey(attr.Locator)?.Value;
                if (string.IsNullOrEmpty(content) == false) {
                    prop.SetValue(this, content);
                }
            }
        }

        public void AdvanceNextStage(ResponseObject obj) {
            _CurrentResponseObject = obj;
            var content = obj?.content.DecodeString();
            foreach (var prop in this.GetType().PublicPropertiesOf().Where(p => p.HasAttribute<PatternLocatorAttribute>())) {
                var plocator = prop.GetCustomAttribute<PatternLocatorAttribute>();
                var pattern = SettingsEngine.SettingByKey(plocator.Locator)?.Value;
                if (plocator.Locator == "ParamsSearchLinkPattern") {
                    if (prop.GetValue(this) == null) {
                        prop.SetValue(this, content?.Matches(pattern).AtIndexOrDefault(Corporate?.Index.To<int>() ?? 0)?.Groups[1].Value);
                    }
                } else if (plocator.Locator == "ParamsCorporateType") {
                    if (prop.GetValue(this) == null) {
                        prop.SetValue(this, obj?.content.Matches(pattern).FirstOrDefault()?.Groups[1].Value);
                    }
                } else if (plocator.Stage == _CurrentStage) {
                    prop.SetValue(this, content?.Matches(pattern).FirstOrDefault()?.Groups[1].Value);
                }
            }
            this._CurrentStage++;
        }

        public void FlushProperties() {
            this._CurrentStage = 3;
            foreach (var prop in this.GetType().PublicPropertiesOf().Where(p => p.HasAttribute<ContentLocatorAttribute>())) {
                prop.SetValue(this, null);
            }
            foreach (var prop in this.GetType().PublicPropertiesOf().Where(p => p.HasAttribute<PatternLocatorAttribute>())) {
                if (prop.GetCustomAttribute<PatternLocatorAttribute>().DoNotFlush == false) {
                    prop.SetValue(this, null);
                }
            }
        }

        public string ParseSubmitUrl() {
            var encurl = "";
            switch (_CurrentStage) {
                case 3:
                    encurl = PaymentUrl;
                    break;
                case 4:
                    encurl = SuggestDataUrl;
                    break;
                case 5:
                    encurl = SuggestRequestUrl;
                    break;
                case 6:
                    encurl = SubmitUrl;
                    break;
                default:
                    encurl = "<NotFound>";
                    break;
            }
            var url = $"https://sube.garanti.com.tr/isube/encurl/{encurl}";
            return url;
        }

        internal IDictionary<string, string> QueryData() {
            switch (_CurrentStage) {
                case 3:
                    return new Dictionary<string, string> {
                        { "targetAreaId", "content"},
                        { "pNavId", "4"},
                        { "cInfoBean.v", Bean}
                    };
                case 4:
                    return new Dictionary<string, string> {
                        { "q", Corporate?.KurumAdi},
                        { "cInfoBean.v", Bean}
                    };
                case 5:
                    return new Dictionary<string, string> {
                        { "advSearchLink", SearchLink},
                        { "cInfoBean.v", Bean}
                    };
                case 6:
                    return new Dictionary<string, string> {
                        { "advSearchText", SearchText},
                        { "kurumTipi", CorporateType.HtmlDecode()},
                        { "kurumAdi", SearchLink},
                        { "faturaTipi", InvoiceType},
                        { "tesisatTipi", SubInvoiceType},
                        { "tesisatNo1", Input?.SearchItem},
                        { "tesisatNo2", Input?.SearchItem2},
                        { "targetAreaId", "content"},
                        { "pNavId", "4"},
                        { "cInfoBean.v", Bean}
                    };
            }
            return null;

        }

        internal string ParseHeaders(out byte[] postData) {
            postData = Encoding.ASCII.GetBytes(QueryData().Aggregate("", (str, n) => $@"{n.Key}={WebUtility.UrlEncode(n.Value)}&{str}").TrimEnd('&'));
            var dict = new Dictionary<string, string> {
                { "_csrf", this.CSRF },
                { "Content-Type", "application/x-www-form-urlencoded" },
                { "Content-Length", postData.Length.ToString() },
                { "Referer", "https://sube.garanti.com.tr/isube/login/login/smspinverify" },
                { "Origin", "https://sube.garanti.com.tr" },
                { "Connection", "keep-alive" },
                { "Host", "sube.garanti.com.tr" },
                { "X-Requested-With", "XMLHttpRequest" }
            };
            return dict.Aggregate("", (str, n) => $@"{n.Key}: {n.Value}{Environment.NewLine}{str}").Trim();
        }
    }
}
