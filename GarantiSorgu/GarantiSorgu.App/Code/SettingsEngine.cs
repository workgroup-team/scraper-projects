﻿using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarantiSorgu.App.Code {

    public class SettingsEngine {

        private static IEnumerable<SettingsModel> _Settings;

        static SettingsEngine() {
            Refresh();
        }

        public static SettingsModel SettingByKey(string key) {
            return _Settings.SingleOrDefault(s => s.Key == key);
        }

        internal static void Refresh() {
            _Settings = DataManager.ReadAll<SettingsModel>();
        }
    }

}
