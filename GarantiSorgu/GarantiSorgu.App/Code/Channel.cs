﻿using GarantiSorgu.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GarantiSorgu.App.Code {

    public class Channel {

        public delegate void SocketHandledHandler(SocketOutput output);
        public event SocketHandledHandler SocketHandled;

        private TcpListener Server { get; set; }
        private IPEndPoint EndPoint { get; set; }
        private Stack<byte[]> MsgQueue = new Stack<byte[]>();
        private bool Listening { get; set; } = false;

        public Channel() {
            var ipAddress = IPAddress.Parse("127.0.0.1");
            EndPoint = new IPEndPoint(ipAddress, SettingsEngine.SettingByKey("Port").Value.To<int>());
            Server = new TcpListener(EndPoint);
            //ServicePointManager.MaxServicePointIdleTime = 100; //100000
        }

        public void StartListening() {
            Listening = true;
            Server.Stop();
            Server.Start();
        }

        public async void ListenAsync(Action<SocketInput, Action<SocketOutput>> listenerHandle) {
            Byte[] bytes = new Byte[65536];
            String incomingData = null;
            while (Listening) {
                await Task.Run(() => {
                    try {
                        var timeout = SettingsEngine.SettingByKey("Timeout").Value.To<int>();
                        incomingData = null;
                        var client = Server.AcceptTcpClient();
                        var tcpStream = client.GetStream();
                        int i;
                        if ((i = tcpStream.Read(bytes, 0, bytes.Length)) != 0) {
                            incomingData = Encoding.ASCII.GetString(bytes, 0, i);
                            var socketInput = SocketInput.Deserialize(incomingData.Replace("\r\n", ""));
                            listenerHandle(socketInput, (output) => {
                                SocketHandled?.Invoke(output);
                                var msg = output?.Serialize().DecodeBytes();
                                MsgQueue.Push(msg);
                            });
                            var to = timeout;
                            while (MsgQueue.Count == 0 && to > 0) { Thread.Sleep(100); to -= 100; }
                            if (MsgQueue.Count == 0) {
                                var errorput = new SocketOutput("zaman_asimi");
                                SocketHandled?.Invoke(errorput);
                                var msg = errorput?.Serialize().DecodeBytes();
                                tcpStream.Write(msg, 0, msg.Length);
                                tcpStream.Flush();
                            }
                            while (MsgQueue.Count > 0) {
                                var msg = MsgQueue.Pop();
                                tcpStream.Write(msg, 0, msg.Length);
                                tcpStream.Flush();
                            }
                            client.Close();
                        }
                    } catch (Exception) {

                    }
                });
            }
        }

        public void Seize() {
            Server.Stop();
            Listening = false;
        }

    }

}
