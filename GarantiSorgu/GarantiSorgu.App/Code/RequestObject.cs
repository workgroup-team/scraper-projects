﻿using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Forms;
using WebBrowser = System.Windows.Controls.WebBrowser;
namespace GarantiSorgu.App.Code {
    public class RequestObject {

        public delegate void OutputParsedHandler(RequestObject requestObject, SocketOutput output);
        public event OutputParsedHandler OutputParsed;

        public WebBrowser Browser { get; private set; }
        public PostParams PParams { get; private set; }
        private bool _RequestInitialized;

        public void LoadVars(WebBrowser browser, PostParams pparams) {
            this.Browser = browser;
            PParams = pparams;
            _RequestInitialized = false;
        }

        public void ReadPParams(WebBrowser wb, CorporateModel corporate, SocketInput input, string bean, int currentStage) {
            PParams.ReadContent(wb, corporate, input, bean, currentStage);
        }

        public void PostCascade(bool cacheExists = false) {
            if (cacheExists) {
                if (SettingsEngine.SettingByKey("UseWebDocumentCache")?.Value.To<bool>() == true) {
                    this.PostCascadeWithCaching();
                } else {
                    this.PostCascadeWithBrowsing();
                }
            } else {
                this.PostCascadeWithBrowsing();
            }
        }

        private void PostCascadeWithBrowsing() {
            this.Browser.LoadCompleted -= Browser_LoadCompleted;
            this.Browser.LoadCompleted += Browser_LoadCompleted;
            if (_RequestInitialized == false) {
                this.Browser.NavigateToParams(PParams);
                _RequestInitialized = true;
            }

        }

        private void Browser_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e) {
            if (PParams.Finalized == false && e.Uri.ToString().StartsWith(PParams.ParseSubmitUrl())) {
                var obj = this.Browser.Body(PParams.CurrentStage, out var content).DeserializeTo<ResponseObject>();
                obj?.SetContent(content);
                PParams.AdvanceNextStage(obj);
                var so = new SocketOutput(obj, PParams);
                OutputParsed?.Invoke(this, so);
                if (PParams.Finalized == false) {
                    this.Browser.NavigateToParams(PParams);
                } else {
                    PParams.FlushProperties();
                }
            }
        }

        private void PostCascadeWithCaching() {
            if (SettingsEngine.SettingByKey("UseWebDocumentCache")?.Value.To<bool>() == false) {
                throw new Exception("UseWebDocumentCache is false; use PostCascade instead");
            }
            while (PParams.Finalized == false) {
                var obj = this.Browser.Body(PParams.CurrentStage, out var content).DeserializeTo<ResponseObject>();
                obj?.SetContent(content);
                PParams.AdvanceNextStage(obj);
                var so = new SocketOutput(obj, PParams);
                OutputParsed?.Invoke(this, so);
            }
        }

    }
}
