﻿using GarantiSorgu.App.Code;
using GarantiSorgu.Core.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace GarantiSorgu.App.Client {
    public class SocketClient {

        private TcpClient Sender;
        private IPEndPoint EndPoint;
        public SocketClient() {
            var ipAddress = IPAddress.Parse("127.0.0.1");
            EndPoint = new IPEndPoint(ipAddress, SettingsEngine.SettingByKey("Port").Value.To<int>());
        }

        public void SendToSocket(SocketInput input) {
            try {
                Sender = new TcpClient();
                Sender.Connect(EndPoint);
                var serverStream = Sender.GetStream();
                byte[] outStream = input.Serialize().DecodeBytes();
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
                Sender.Close();
            } catch {

            }
        }

        public void End() {
            Sender?.Close();
        }

    }
}
