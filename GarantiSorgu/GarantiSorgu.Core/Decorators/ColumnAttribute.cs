﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarantiSorgu.Core.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    sealed class ColumnAttribute : Attribute {

        public ColumnAttribute(string columnName) {
            this.ColumnName = columnName;
        }

        public string ColumnName { get; private set; }
    }
}
