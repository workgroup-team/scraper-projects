﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace GarantiSorgu.Core.Data {
    public static class Extensions {

        public static IEnumerable<PropertyInfo> PublicPropertiesOf<TModel>(this TModel model) {
            return typeof(TModel).PublicPropertiesOf();
        }

        public static IEnumerable<PropertyInfo> PublicPropertiesOf(this Type modelType) {
            return modelType.GetProperties(BindingFlags.Instance | BindingFlags.Public);
        }

        public static string DescriptionOf<TEntity>(this PropertyInfo prop) {
            return typeof(TEntity).PublicPropertiesOf()
                .Where(p => p.HasAttribute<DescriptionAttribute>())
                .SingleOrDefault(p => p.Name == prop.Name)?.GetCustomAttribute<DescriptionAttribute>().Description;
        }

        public static bool HasAttribute<TAttr>(this PropertyInfo prop)
            where TAttr : Attribute {
            return prop.GetCustomAttribute<TAttr>() != null;
        }

        public static object StronglyType(this object self) {
            var t = self.GetType();
            if (t.IsValueType) {
                if (t.Equals(typeof(int)) || t.Equals(typeof(int?))) {
                    return Convert.ToInt32(self);
                } else if (t.Equals(typeof(long)) || t.Equals(typeof(long?))) {
                    return Convert.ToInt64(self);
                } else if (t.Equals(typeof(string))) {
                    return self.ToString();
                }
            }
            return self;
        }

        public static bool IsAssigned(this object value) {
            return value != null && value.GetType().IsValueType ? (value.ToString() != "0" || !value.Equals(Guid.Empty) || !value.Equals(String.Empty)) : !ReferenceEquals(null, value);
        }

        public static string StripTrailing(this string self, string suffix) {

            var that = self;
            foreach (var c in suffix.Reversed()) {
                that = that.TrimEnd(c);
            }
            return that;
        }

        public static string Reversed(this string self) {
            var ca = self.ToCharArray();
            Array.Reverse(ca);
            return new String(ca);
        }

        public static string Put(this string format, params object[] args) {
            return string.Format(format, args.ToArray());
        }

        public static string PutBetween(this string self, string delimeter) {
            return $"{delimeter}{self}{delimeter}";
        }

        public static string ToEscaped(this string self) {
            return self.Replace("\"", "\\\"").Replace("'", "\\'").Trim().Replace("\r\n", "").Replace("\n", "");
        }

        public static TModel DeserializeTo<TModel>(this string str)
            where TModel : class {
            try {
                return new JavaScriptSerializer().Deserialize<TModel>(str);
            } catch (ArgumentException) {
                return null;
            };
        }

        public static TResult To<TResult>(this object obj) {
            var t = typeof(TResult);
            try {
                if (t.GetInterface("IConvertible") != null) {
                    return (TResult)Convert.ChangeType(obj, t);
                } else if (t.Equals(typeof(int))) {
                    return (TResult)((object)Convert.ToInt32(obj));
                } else if (t.Equals(typeof(bool))) {
                    return (TResult)((object)Convert.ToBoolean(obj));
                } else if (t.Equals(typeof(long))) {
                    return (TResult)((object)Convert.ToInt64(obj));
                } else if (t.Equals(typeof(double))) {
                    return (TResult)((object)Convert.ToDouble(obj));
                } else {
                    return (TResult)obj;
                }
            } catch (Exception) {
                return default(TResult);
            }
        }

        public static IEnumerable<Match> Matches(this string self, string pattern) {
            var r = new Regex(pattern);
            var ms = r.Matches(self);
            return ms.OfType<Match>().OrderBy(m => m.Index);
        }
        public static string DecodeString(this string str) {
            var utf8msg = Encoding.UTF8.GetBytes(str);
            var bytes = Encoding.Convert(Encoding.UTF8,
                Encoding.GetEncoding("Windows-1254"), utf8msg);
            return Encoding.UTF8.GetString(bytes, 0, bytes.Length);
        }

        public static byte[] DecodeBytes(this string str) {
            var utf8msg = Encoding.UTF8.GetBytes(str);
            var bytes = Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding("ISO-8859-1"), utf8msg);
            return bytes;
        }

        public static string DecodeUTF8(this string str) {
            return Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(str));
        }

        public static T AtIndexOrDefault<T>(this IEnumerable<T> self, int n)
            where T : class {
            if (n < self.Count()) {
                return self.ElementAt(n);
            } else {
                return default(T);
            }
        }
        public static string HtmlDecode(this string self) {
            return WebUtility.HtmlDecode(self);
        }
        public static string GetEncodedUrl(this string textToEncode) {
            string temp = textToEncode;

            for (int i = 0; i < temp.Length; i++) {
                if (temp[i] == 'ç') {
                    temp = temp.Replace("ç", "%C3%A7");
                } else if (temp[i] == 'Ç') {
                    temp = temp.Replace("Ç", "%C3%87");
                } else if (temp[i] == 'ğ') {
                    temp = temp.Replace("ğ", "%C4%9F");
                } else if (temp[i] == 'Ğ') {
                    temp = temp.Replace("Ğ", "%C4%9E");
                } else if (temp[i] == 'ı') {
                    temp = temp.Replace("ı", "%C4%B1");
                } else if (temp[i] == 'İ') {
                    temp = temp.Replace("İ", "%C4%B0");
                } else if (temp[i] == 'ö') {
                    temp = temp.Replace("ö", "%C3%B6");
                } else if (temp[i] == 'Ö') {
                    temp = temp.Replace("Ö", "%C3%96");
                } else if (temp[i] == 'ş') {
                    temp = temp.Replace("ş", "%C5%9F");
                } else if (temp[i] == 'Ş') {
                    temp = temp.Replace("Ş", "%C5%9E");
                } else if (temp[i] == 'ü') {
                    temp = temp.Replace("ü", "%C3%BC");
                } else if (temp[i] == 'Ü') {
                    temp = temp.Replace("Ü", "%C3%9C");
                }
            }

            return temp;
        }

        public static async Task<TcpClient> AcceptAsync(this TcpListener listener, CancellationToken ct) {
            using (ct.Register(listener.Stop)) {
                try {
                    return await listener.AcceptTcpClientAsync();
                } catch (SocketException e) when (e.SocketErrorCode == SocketError.Interrupted) {
                    return null;
                } catch (ObjectDisposedException) when (ct.IsCancellationRequested) {
                    return null;
                }
            }
        }
    }
}
