﻿namespace GarantiSorgu.Core.Data {

    public interface IModel {

        string TableName { get; }
        string SelectAllClause { get; }

    }

}