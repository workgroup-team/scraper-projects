﻿using GarantiSorgu.Core.Decorators;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GarantiSorgu.Core.Data {

    public static class DataManager {

        public static IEnumerable<TModel> ReadAll<TModel>()
           where TModel : IModel {
            using (var eng = new SqliteEngine()) {
                using (var reader = eng.SelectReaderFor<TModel>()) {
                    while (reader.Read()) {
                        var entity = Activator.CreateInstance<TModel>();
                        foreach (var prop in typeof(TModel).PublicPropertiesOf()
                            .Where(p => p.HasAttribute<ColumnAttribute>())) {
                            prop.SetValue(entity, reader[prop.GetCustomAttribute<ColumnAttribute>().ColumnName].StronglyType());
                        }
                        yield return entity;
                    }
                }
            }
        }

    }
}
