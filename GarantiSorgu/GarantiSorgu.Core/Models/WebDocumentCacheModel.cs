﻿using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarantiSorgu.Core.Models {
    public class WebDocumentCacheModel : BaseModel<WebDocumentCacheModel>, IModel {

        public string TableName => "WebDocumentCache";

        [Column("ID"), PK]
        public long CacheID { get; set; }
        [Column("Key")]
        public string Key { get; set; }
        [Column("Body")]
        public string Body { get; set; }
        [Column("Content")]
        public string Content { get; set; }

    }
}
