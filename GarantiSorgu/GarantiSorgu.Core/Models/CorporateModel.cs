﻿using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarantiSorgu.Core.Models {
    public class CorporateModel : BaseModel<CorporateModel>, IModel {

        public string TableName => "Corporates";

        [Column("ID"), PK]
        public long KurumID { get; set; }
        [Column("CorporateName")]
        public string KurumAdi { get; set; }
        [Column("BranchName")]
        public string BransAdi { get; set; }
        [Column("FilterIndex")]
        public long Index { get; set; }
        [Column("InvoiceTypeIndex")]
        public long FaturaTipIndex { get; set; }
        [Column("MaxLength")]
        public long UzunlukMax { get; set; }

    }
}
