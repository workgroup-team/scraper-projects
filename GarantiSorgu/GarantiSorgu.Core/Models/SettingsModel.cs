﻿using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarantiSorgu.Core.Models {
    public class SettingsModel : BaseModel<SettingsModel>, IModel {

        public string TableName => "Settings";

        [Column("ID"), PK]
        public long SettingID { get; set; }
        [Column("Key")]
        public string Key { get; set; }
        [Column("Value")]
        public string Value { get; set; }

    }
}
