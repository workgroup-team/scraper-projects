﻿using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarantiSorgu.Core.Models {
    public class AccountModel : BaseModel<AccountModel>, IModel {

        public string TableName => "Accounts";

        [Column("ID"), PK]
        public long HesapID { get; set; }
        [Column("BankName")]
        public string Banka { get; set; }
        [Column("IsCorporate")]
        public long KurumsalMi { get; set; }
        [Column("AccountName")]
        public string HesapAdi { get; set; }
        [Column("CorporateCode")]
        public string KurumKodu { get; set; }
        [Column("AccountNumber")]
        public string HesapNo { get; set; }
        [Column("Password")]
        public string Sifre { get; set; }
        [Column("IsActive")]
        public long AktifMi { get; set; }

    }
}
