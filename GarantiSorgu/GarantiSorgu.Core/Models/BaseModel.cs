﻿
using GarantiSorgu.Core.Data;
using GarantiSorgu.Core.Decorators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GarantiSorgu.Core.Models {
    public class BaseModel<TModel>
        where TModel : class, IModel {

        public string SelectAllClause => $"SELECT * FROM {Activator.CreateInstance<TModel>().TableName}";

        public TModel UpsertSelf<TProp>(Expression<Func<TModel, TProp>> keySelector) {
            var columns = new StringBuilder();
            var parameters = new StringBuilder();
            var set = new StringBuilder();
            var props = typeof(TModel).PublicPropertiesOf()
                .Where(p => p.HasAttribute<ColumnAttribute>());
            foreach (var prop in props.Where(p => !p.HasAttribute<PKAttribute>())) {
                var value = prop.GetValue(this);
                if (value.IsAssigned()) {
                    var colName = prop.GetCustomAttribute<ColumnAttribute>().ColumnName;
                    columns.AppendFormat("[{0}], ", colName);
                    parameters.AppendFormat("@{0}, ", colName);
                    set.AppendFormat("[{0}]=@{0}, ", colName);
                }
            }
            PropertyInfo uqkey = null;
            if (keySelector.Body is MemberExpression keymem) {
                uqkey = keymem.Member as PropertyInfo;
            } else {
                throw new Exception("Selector should be member expression");
            }
            var key = uqkey.GetValue(this);
            var clause1 = $"UPDATE {Activator.CreateInstance<TModel>().TableName} SET {set.ToString().StripTrailing(", ")} WHERE [{uqkey.GetCustomAttribute<ColumnAttribute>().ColumnName}]=@__key";
            var clause2 = $"INSERT INTO {Activator.CreateInstance<TModel>().TableName} ({columns.ToString().StripTrailing(", ")}) SELECT {parameters.ToString().StripTrailing(", ")} WHERE (Select Changes() = 0)";
            var finalClause = $"{clause1}; {clause2};";
            using (var eng = new SqliteEngine()) {
                if (eng.ExecuteClause(finalClause, (cmd) => {
                    foreach (var p in props.Where(p => !p.HasAttribute<PKAttribute>())) {
                        var colName = p.GetCustomAttribute<ColumnAttribute>().ColumnName;
                        var value = p.GetValue(this);
                        if (value.IsAssigned()) {
                            cmd.Parameters.AddWithValue(colName, value);
                        }
                    }
                    cmd.Parameters.AddWithValue("__key", key);
                }) == true) {
                    var insertedID = eng.ExecuteIdentity();
                    if (insertedID > 0) {
                        props.SingleOrDefault(p => p.HasAttribute<PKAttribute>()).SetValue(this, insertedID);
                    }
                } else {
                    throw new Exception($"Upsert Clause Failed To Insert: {finalClause}");
                }
            }
            return this as TModel;
        }

    }
}
