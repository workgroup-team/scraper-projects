﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GarantiSorgu.Core.Data;
using System.Data;
using GarantiSorgu.Core.Models;
using System.Diagnostics;

namespace GarantiSorgu.Tests.UnitTests {
    [TestClass]
    public class DataTests {
        [TestMethod]
        public void ConnectionOK() {

            using (var eng = new SqliteEngine()) {
                Assert.AreEqual(ConnectionState.Open, eng.State);
            }

        }

        [TestMethod]
        public void DataManagerReads() {
            foreach (var hesap in DataManager.ReadAll<AccountModel>()) {
                Assert.AreNotEqual("0", hesap.HesapID);
            }
        }


        [TestMethod]
        public void UpsertSucceeds() {
            var cache = 
                new WebDocumentCacheModel { Key = "willdelete", Body = "völdkfpğwsdkfpowawkpfwpfkwepfkwpeofkpeof", Content = "vmdskdms" }.UpsertSelf(ch => ch.Key);
            Assert.AreNotEqual(0, cache.CacheID);
            Debug.Write(cache.CacheID);
        }

    }
}
